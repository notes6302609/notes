package dao;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import pojos.*;

public interface IUserDao {
	List<User> getSelectedUsers(Role role, Date begin, Date end) throws SQLException;

	String registerUser(User newUser) throws SQLException;

	String updateUserDetails(int userId, String newPwd, double offsetAmount) throws SQLException;
	User authenticateUser(String email,String password) throws SQLException;
}
