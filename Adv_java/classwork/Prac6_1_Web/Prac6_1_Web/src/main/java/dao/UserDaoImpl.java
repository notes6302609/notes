package dao;
import java.sql.Connection;
import  java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import pojos.Role;
import pojos.User;
import static utils.DBUtils.*;
public class UserDaoImpl  implements IUserDao{
private Connection cn;
private PreparedStatement pst1, pst2, pst3,pst4;
	public UserDaoImpl() throws SQLException, ClassNotFoundException{
	  
		cn=getConnection();
		String selectSql = "select id,name,reg_amt,reg_date from users where role=? "
				+ "and reg_date between ? and ?";
	
		
		pst1=cn.prepareStatement(selectSql,ResultSet.TYPE_SCROLL_INSENSITIVE,ResultSet.CONCUR_READ_ONLY);
		
		String insertSql="insert into users values(default,?,?,?,?,?,?)";
		pst2=cn.prepareStatement(insertSql);
		String updateSql = "update users set password=?,reg_amt=reg_amt+? where id=?";
		pst3=cn.prepareStatement(updateSql);
		System.out.println("user dao created !");
		pst4=cn.prepareStatement("select * from users where email=? and password=?");
		System.out.println("user dao created !");
	}
	
	@Override
	public List<User> getSelectedUsers(Role role, Date begin, Date end) throws SQLException {
		List<User>users=new ArrayList<>();
		pst1.setString(1, role.name());
		pst1.setDate(2, begin);
		pst1.setDate(3,end);
		
		try(ResultSet rst=pst1.executeQuery()){
			
			while(rst.next()) {
		    
				
		}
		while(rst.previous()) {
			users.add(new User(rst.getInt(1),rst.getString(2),rst.getDouble(3),rst.getDate(4)));
		}
		}
		return users;
	}
	public void cleanUp() throws SQLException
	{
		if(pst1 != null)
			pst1.close();
		if(pst2!=null)
			pst2.close();
		if(pst3!=null)
			pst3.close();
		if(pst4!=null)
			pst4.close();
		System.out.println("User Dao Object Cleaned Up");
		
	}

	@Override
	public String registerUser(User newUser) throws SQLException {
		pst2.setString(1,newUser.getName());
		pst2.setString(2, newUser.getEmail());
		pst2.setString(3, newUser.getPassword());
		pst2.setDouble(4, newUser.getRegAmount());
		pst2.setDate(5, newUser.getRegDate());
		pst2.setString(6, newUser.getUserRole().name());
	  int rowCount=pst2.executeUpdate();
	  if(rowCount==1)
		  return "User Registerted SuccesfullyðŸ˜�ðŸ˜�";
		return "User Registration FailedðŸ˜­ðŸ˜­ðŸ˜­";
	}
	public String updateUserDetails(int userId, String newPwd, double offsetAmount) throws SQLException {
	
		pst3.setString(1, newPwd);
		pst3.setDouble(2, offsetAmount);
		pst3.setInt(3, userId);
		// DML : execUpdate
		int rowCount = pst3.executeUpdate();
		if (rowCount == 1)
			return "User Details Updated SuccessfullyðŸ˜�ðŸ˜�ðŸ˜� .....";
		return "Updation of user details failed ðŸ˜¢ðŸ˜¢ðŸ˜¢!!!!!!!!!!!!!!!!!!!!!!!";

	}

	@Override
	public User authenticateUser(String email, String password) throws SQLException {
	
		pst4.setString(1, email);
		pst4.setString(2, password);
		try(ResultSet rst=pst4.executeQuery()){
		
			if(rst.next())
			{ System.out.println("User Found");
				return new User(rst.getInt(1),rst.getString(2),
						email,password,rst.getDouble(5),rst.getDate(6),Role.valueOf(rst.getString(7)));}
		}
		return null;
	}

}
