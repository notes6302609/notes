package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import pojos.Topic;
import static utils.DBUtils.*;

public class TopicDaoImpl implements ITopicDao {

	private Connection cn;
	private PreparedStatement pst1;
	
	public TopicDaoImpl() throws SQLException{
	
		cn=getConnection();
		pst1=cn.prepareStatement("select * from topics");
		System.out.println("topic dao created....");		
	}

	@Override
	public List<Topic> getAllTopics() throws SQLException {
		List<Topic> topicList=new ArrayList<>();
		try(ResultSet rst=pst1.executeQuery())
		{
			while(rst.next())
				topicList.add(new Topic(rst.getInt(1), rst.getString(2)));
		}
		return topicList;
	}

	public void cleanUp() throws SQLException
	{
		if(pst1 != null)
			pst1.close();
		System.out.println("topic dao cleaned up");
		
	}

}
