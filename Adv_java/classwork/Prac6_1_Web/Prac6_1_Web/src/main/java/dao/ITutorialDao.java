package dao;

import java.sql.SQLException;
import java.util.List;

import pojos.Tutorial;

public interface ITutorialDao {

	List<String> getTutorialNamesByTopicId(int topicId) throws SQLException;

	Tutorial getTutorialDetailsByName(String tutName) throws SQLException;

	String updateTutorialVisitsByName(String tutName)  throws SQLException;
	
	
}