package dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import pojos.Tutorial;

import static utils.DBUtils.*;

public class TutorialDaoImpl implements ITutorialDao {

	private Connection cn;
	private PreparedStatement pst1, pst2, pst3;

	public TutorialDaoImpl() throws SQLException {

		cn = getConnection();

		pst1 = cn.prepareStatement("select name from tutorials where topic_id=?");
		pst2 = cn.prepareStatement("select * from tutorials where name=?");
		pst3 = cn.prepareStatement("update tutorials set visits=visits+1 where name=?");
		System.out.println("tut dao created...");
	}

	@Override
	public Tutorial getTutorialDetailsByName(String tutName) throws SQLException {

		pst2.setString(1, tutName);
		try (ResultSet rst = pst2.executeQuery()) {
			/*
			 * tutorialId, String tutName, String author, Date publishDate, int visits,
			 * String contents, int topicId
			 */
			if (rst.next())
				return new Tutorial(rst.getInt(1), tutName, rst.getString(3), rst.getDate(4), rst.getInt(5),
						rst.getString(6), rst.getInt(7));
		}
		return null;
	}

	@Override
	public String updateTutorialVisitsByName(String tutName) throws SQLException {
		
		return null;
	}

	@Override
	public List<String> getTutorialNamesByTopicId(int topicId) throws SQLException {
		List<String> names = new ArrayList<>();
		// set IN param : topic id
		pst1.setInt(1, topicId);
		try (ResultSet rst = pst1.executeQuery()) {
			while (rst.next())
				names.add(rst.getString(1));
		}
		return names;
	}

	public void cleanUp() throws SQLException {
		if (pst1 != null)
			pst1.close();
		System.out.println("Tutorial Dao Cleaned up!!!");
	}

}
