package utils;
import java.sql.*;
public class DBUtils {
 private static Connection connection;
 public static Connection openConnection() throws ClassNotFoundException, SQLException
 {
//	 Class.forName("com.mysql.cj.jdbc.Driver");
	 String url = "jdbc:mysql://localhost:3306/sunbeam22?useSSL=false&allowPublicKeyRetrieval=true";
	
	 connection= DriverManager.getConnection(url,"root","manager");
	 return connection;
 }
 public static Connection getConnection() {
	 return connection;
 }
 public static void closeConnection() throws SQLException
	{
		if(connection != null)
			connection.close();
	}
}
