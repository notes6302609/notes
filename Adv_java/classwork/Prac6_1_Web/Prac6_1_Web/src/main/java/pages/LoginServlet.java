package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TopicDaoImpl;
import dao.TutorialDaoImpl;
import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;
import static utils.DBUtils.*;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(value="/authenticate" ,loadOnStartup = 1)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDaoImpl userDao;
	private TopicDaoImpl topicDao;
	private TutorialDaoImpl tutDao;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		System.out.println("in Init"+Thread.currentThread()+" "+getClass());
		try {
			openConnection();
			System.out.println("Conection Opened In Login Thread!!!!!");
			userDao=new UserDaoImpl();
			topicDao=new TopicDaoImpl();
			tutDao=new TutorialDaoImpl();
			
		} catch (ClassNotFoundException | SQLException e) {
		throw new ServletException("err in init"+getClass().getName(),e);
		
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
	try {
		if(userDao != null)
		{	userDao.cleanUp();
		}
		if(topicDao!=null)
		{
			topicDao.cleanUp();
			
		}
		if(tutDao!=null)
		{
			tutDao.cleanUp();
			
		}
		closeConnection();
		System.out.println("Connection Closed!!!!!!!");
	} catch (Exception e) {
		throw new RuntimeException("err in destroy of"+getClass().getName());
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("In Post of LoginServlet....");
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter()){
			String email =request.getParameter("em");
			String password=request.getParameter("pass");
			try {
				User user = userDao.authenticateUser(email, password);
				if(user==null)
				{	pw.print("<h5>Invalid UserName Or Password<h5>");
				pw.print("<h5><a href='login.html'>Retry</a></h5>");}
				else{
   HttpSession session=request.getSession();
   System.out.println("Login Page Session is new===>"+session.isNew());
   System.out.println("Session Id==>"+session.getId());
   session.setAttribute("user_details", user);
   session.setAttribute("topic_dao", topicDao);
   session.setAttribute("tut_dao", tutDao);
					if(user.getUserRole()==Role.ADMIN) {
						
						response.sendRedirect("admin");
					}
					else
					{
						response.sendRedirect("topics");
					}
				}
			} catch (SQLException e) {
				
				throw new ServletException("err in dopost"+getClass().getName(),e);
			}
		}
	}

}
