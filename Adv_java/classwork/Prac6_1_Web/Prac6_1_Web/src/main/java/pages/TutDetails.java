package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TutorialDaoImpl;
import pojos.Tutorial;

/**
 * Servlet implementation class TutDetails
 */
@WebServlet("/tutorial_details")
public class TutDetails extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  response.setContentType("text/html");
	  HttpSession hs=request.getSession();
		TutorialDaoImpl tutDao=(TutorialDaoImpl) hs.getAttribute("tut_dao");
		Tutorial t=null;
		if(tutDao != null)
		{
			
			String tutname=request.getParameter("tut_name");
			System.out.println(tutname);
		
		try {
			
			t = tutDao.getTutorialDetailsByName(tutname);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try(PrintWriter pw=response.getWriter()){
		if(t!=null) {
			pw.write("<h1>TUTS DETAILS</h1>");
			pw.write(t.toString());
            pw.write("<h1><a href=logout>Logout</a></h1>");
			
		}
		}
		}
	}

}
