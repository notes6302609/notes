package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TutorialDaoImpl;

/**
 * Servlet implementation class TutorialsServlet
 */
//URL : http://localhost:8080/day6.1/tutorials?topic_id=4
@WebServlet("/tutorials")
public class TutorialsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
	
		try(PrintWriter pw=response.getWriter())
		{
			
			HttpSession hs=request.getSession();
			System.out.println("in tutorial page " + hs.isNew());
			System.out.println("session id " + hs.getId());
			
			TutorialDaoImpl tutDao=(TutorialDaoImpl) hs.getAttribute("tut_dao");
			if(tutDao != null)
			{
				
				int topicId=Integer.parseInt(request.getParameter("topic_id"));
			
				for (String name : tutDao.getTutorialNamesByTopicId(topicId))
				
					pw.print("<h5><a href='tutorial_details?tut_name="+name+"'>"+name+"</a></h5>");
					
			}
			else
				pw.print("<h5> NO Cookies !!!!!!! Session Tracking Failed !!!!!!!!!!!!!!!!</h5>");
		} catch (Exception e) {
			throw new ServletException("err in do-get of "+getClass(), e);
		}
	}

}
