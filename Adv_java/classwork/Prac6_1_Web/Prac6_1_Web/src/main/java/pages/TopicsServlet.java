package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.TopicDaoImpl;
import pojos.Topic;
import pojos.User;

/**
 * Servlet implementation class TopicsServlet
 */
@WebServlet("/topics")
public class TopicsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopicsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	System.out.println("In topics Do get to get all  topics from db...");
		response.setContentType("text/html");
		try(PrintWriter pw=response.getWriter()){
			pw.print("<h5>In topics page....</h5>");
		
			HttpSession hs = request.getSession();
			System.out.println("in topic page " + hs.isNew());
			System.out.println("session id " + hs.getId());
		
			User userDetails = (User) hs.getAttribute("user_details");
			if (userDetails != null) {
				pw.print("<h5> User details from HttpSession " + userDetails + "</h5>");
				
				TopicDaoImpl topicDao = (TopicDaoImpl) hs.getAttribute("topic_dao");
			
				List<Topic> topics = topicDao.getAllTopics();
				
				pw.print("<form action='tutorials'>");
				for (Topic t : topics)
					pw.print("<h5><input type='radio' name='topic_id' value='" + t.getTopicId() + "'/>"
							+ t.getTopicName() + "</h5>");
				pw.print("<h5><input type='submit' value='Choose A Topic'/></h5>");
				pw.print("</form>");

			} else
				pw.print("<h5> No Cookies !!!!!!!!!!!!! Session Management Failed !!!!!!!!!!!!!!!!!!!!!</h5>");

		} catch (Exception e) {
			e.printStackTrace();
		}
		}
	

}
