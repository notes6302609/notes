package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import pojos.Candidate;

import static utils.DBUtils.*;

public class CandidateDaoImpl implements ICandidateDao {
private Connection cn;
private PreparedStatement pst1,pst2,pst3;
public CandidateDaoImpl() throws SQLException {
	cn=getConnetion();
String sel_Cand_Query="select * from candidates";
pst1=cn.prepareStatement(sel_Cand_Query);
String sel_Cand_Query_for_Top_Two="select * from candidates order by votes desc limit 2";
pst2=cn.prepareStatement(sel_Cand_Query_for_Top_Two);
String AddVote ="update candidates  set votes=(votes+1) where id=?";
pst3=cn.prepareStatement(AddVote);
}
	@Override
	public List<Candidate> getCandidates() {
		List<Candidate> candidates=new ArrayList<>();
		try(ResultSet rst=pst1.executeQuery()) {
			while(rst.next()) {
//				| id | name    | party    | votes
			candidates.add(new Candidate(rst.getInt("id"),rst.getString("name"),rst.getString("party"),rst.getInt("votes") ));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidates;
	}
	public List<Candidate> getTopTwoCandidates() {
		List<Candidate> candidates=new ArrayList<>();
		try(ResultSet rst=pst2.executeQuery()) {
			while(rst.next()) {
//				| id | name    | party    | votes
			candidates.add(new Candidate(rst.getInt("id"),rst.getString("name"),rst.getString("party"),rst.getInt("votes") ));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return candidates;
	}
	public boolean AddVote(int id) throws SQLException {
		pst3.setInt(1, id);
		int rowsAffected=pst3.executeUpdate();
		if(rowsAffected==0)
			return false;
		return true;
	}
}
