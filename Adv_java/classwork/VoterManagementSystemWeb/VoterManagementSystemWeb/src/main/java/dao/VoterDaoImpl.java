package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import pojos.*;

import  static utils.DBUtils.*;
public class VoterDaoImpl  implements IVoterDao{
private Connection connection;
private PreparedStatement pst1,pst2;
	public VoterDaoImpl() throws SQLException {
		connection=getConnetion();
		String loginQuery="select * from voters where name=? and password=? ";
		pst1=connection.prepareStatement(loginQuery);
		String changeStatusQuery="Update voters set status=true where id=?";
				pst2=connection.prepareStatement(changeStatusQuery);	
	}

	@Override
	public Voter Login(Voter v) throws SQLException {
//	 id | name    | email             | password | status | role
		pst1.setString(1, v.getName());
		pst1.setString(2,v.getPassword());
		try(ResultSet rst=pst1.executeQuery()) {
		    if(rst.next())
		    	return new Voter(rst.getInt(1), rst.getString("name"),rst.getString("email"), rst.getString("password") ,
		    			rst.getBoolean("status"),Role.valueOf(rst.getString("role").toUpperCase()));	
		    }
		return null;
	}

	public void changeStatus(int id) throws SQLException {
		pst2.setInt(1, id);
	  pst2.executeUpdate();
		
	}

}
