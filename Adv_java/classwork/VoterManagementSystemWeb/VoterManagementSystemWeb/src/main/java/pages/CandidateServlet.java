package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Candidate;

/**
 * Servlet implementation class CandidateServlet
 */
@WebServlet("/adminhome")
public class CandidateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  private CandidateDaoImpl cdi;
  private VoterDaoImpl vdi;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			cdi=new CandidateDaoImpl();
			vdi=new VoterDaoImpl();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	response.setContentType("text/html");
	List<Candidate>topTwo=cdi.getTopTwoCandidates();
		try(PrintWriter pw=response.getWriter()){
			pw.write("<!-- CSS only -->\r\n"
					+ "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx\" crossorigin=\"anonymous\">");
			pw.write("<!-- JavaScript Bundle with Popper -->\r\n"
					+ "<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa\" crossorigin=\"anonymous\"></script>");
			pw.write("<br><center><h1>!!!!!  Top Two Candidates  !!!!!</h1></center>");
			pw.write("<table class='table table-bordered table-striped mt-5'><tr><th>ID</th><th>Name</th><th>Party</th><th>Votes</th></tr>");
			for (Candidate candidate : topTwo) {
				pw.write("<tr>");
				pw.write("<td>"+candidate.getId()+"</td>");
				pw.write("<td>"+candidate.getName()+"</td>");
				pw.write("<td>"+candidate.getParty()+"</td>");
				pw.write("<td>"+candidate.getVotes()+"</td>");
				pw.write("</tr>");
			}
			pw.write("</table>");
			pw.write("<center><a href=\"index.html\" class=\"btn btn-success rounded-pill mt-3\">Go To Home</a></center>") ;
			
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  String vote=request.getParameter("candidate");
	  System.out.println(vote);
	  Cookie[] c=request.getCookies();
	  int voterId=Integer.parseInt(c[0].getValue());
	  boolean voted=false;
	  try {
	 voted=cdi.AddVote(Integer.parseInt(vote));
	 
	} catch (NumberFormatException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  try(PrintWriter pw=response.getWriter()) {
			pw.write("<!-- CSS only -->\r\n"
					+ "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx\" crossorigin=\"anonymous\">");
			pw.write("<!-- JavaScript Bundle with Popper -->\r\n"
					+ "<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa\" crossorigin=\"anonymous\"></script>");
			if(voted) {
				try {
					vdi.changeStatus(voterId);
				} catch (SQLException e) {
				
					e.printStackTrace();
				}
		pw.write("<br><br><br><center><h1>Hurray You  voted Successfully !!!!!!!!!!</h1></center>");	
			}
			else {
				pw.write("<br><br><br><center><h1>Oops Voting Failed !!!!!!!!!!</h1></center>");	
			}
			pw.write("<center><a href=\"index.html\" class=\"btn btn-success rounded-pill mt-3\">Go To Home</a></center>") ;
	  }
	  }
	}


