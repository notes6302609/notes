package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CandidateDaoImpl;
import dao.VoterDaoImpl;
import pojos.Candidate;

/**
 * Servlet implementation class VoterHomeServlet
 */
@WebServlet("/votingpage")
public class VoterHomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  private VoterDaoImpl vdi;
  private CandidateDaoImpl cdi;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
		try {
			vdi=new VoterDaoImpl();
			cdi=new CandidateDaoImpl();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		Cookie[] c=request.getCookies();
		List<Candidate> candidates=cdi.getCandidates();
		try(PrintWriter pw=response.getWriter()) {
			if(Boolean.parseBoolean(c[1].getValue())) {
				pw.write("<!-- CSS only -->\r\n"
						+ "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx\" crossorigin=\"anonymous\">");
				pw.write("<!-- JavaScript Bundle with Popper -->\r\n"
						+ "<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa\" crossorigin=\"anonymous\"></script>");
			pw.write("<br><br><br><center><h1>Oops You have already voted !!!!!!!!!!</h1></center>");	
			pw.write("<center><a href=\"index.html\" class=\"btn btn-success rounded-pill mt-3\">Go To Home</a></center>") ;
			}
			else
			{
				pw.write("<!-- CSS only -->\r\n"
						+ "<link href=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css\" rel=\"stylesheet\" integrity=\"sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx\" crossorigin=\"anonymous\">");
				pw.write("<!-- JavaScript Bundle with Popper -->\r\n"
						+ "<script src=\"https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js\" integrity=\"sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa\" crossorigin=\"anonymous\"></script>");
				pw.write("<center><h1>*****Give Your Valuable Vote*****</h1></center>");	
		pw.write("<form action='adminhome'  method=post>");
				pw.write("<center><table class='table table-striped table-bordered mt-5'>");
				for (Candidate candidate : candidates) {
					pw.write("<tr>");
					pw.write("<td>"+candidate.getId()+"</td><td>");
					pw.write("<input type=radio id="+candidate.getId()+" name=candidate  value="+candidate.getId()+">");
					pw.write(" <label for="+candidate.getId()+">"+candidate.getName()+"</label></td>");
					pw.write("</tr>");
					
				}
				pw.write("</table></center>");
				pw.write("<center><button type=submit class='btn btn-success rounded rounded-pill'>Confirm Vote</button></center>");
		pw.write("</form>");
			}
					
		} catch (Exception e) {
			// TODO: handle exception
		}
			
		}
		
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
