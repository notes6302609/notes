package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.VoterDaoImpl;
import pojos.Role;
import pojos.Voter;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet(value="/authenticate", loadOnStartup=1)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  private  VoterDaoImpl vdi;
	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init() throws ServletException {
	  try {
		vdi=new VoterDaoImpl();
	} catch (SQLException e) {
	
		e.printStackTrace();
	}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String  name=request.getParameter("name");
		String  password=request.getParameter("password");
		Cookie c1=null;
		Cookie c2=null;
		try {
			Voter v=vdi.Login(new Voter(name,password));
			if(v!=null) {
				if(v.getRole()==Role.ADMIN) {
					response.sendRedirect("adminhome");
				}
				else {
		          c1=new Cookie("voterid",""+v.getId());
		          c2=new Cookie("votestatus",Boolean.toString(v.getStatus()));
		          response.addCookie(c1);
		          response.addCookie(c2);
					response.sendRedirect("votingpage");
				}
			}
			else {
				try(PrintWriter pw=response.getWriter()){
				 pw.write("<h1>Sorrry User Not Found<h1>");
				}
			}
		} catch (SQLException e) {
		
			e.printStackTrace();
		}
	}

}
