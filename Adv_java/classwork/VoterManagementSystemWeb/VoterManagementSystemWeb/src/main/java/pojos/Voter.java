package pojos;

public class Voter {
private int id;
private String name;
private String email;
private String password;
private boolean status;
private Role role;
public int getId() {
	return id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}

public boolean getStatus() {
	return status;
}
public void setStatus(boolean status) {
	this.status = status;
}
public Role getRole() {
	return role;
}
public void setRole(Role role) {
	this.role = role;
}

public Voter(String name, String email, String password, boolean status, Role role) {
	super();
	this.name = name;
	this.email = email;
	this.password = password;
	this.status = status;
	this.role = role;
}
public Voter(String name, String password) {
	super();
this.name=name;
	this.password = password;
}
public Voter(int id, String name, String email, String password, boolean status, Role role) {
	super();
	this.id = id;
	this.name = name;
	this.email = email;
	this.password = password;
	this.status = status;
	this.role = role;
}
@Override
public String toString() {
	return "Voter [id=" + id + ", name=" + name + ", email=" + email + ", password=" + password + ", status=" + status
			+ ", role=" + role + "]";
}




}
