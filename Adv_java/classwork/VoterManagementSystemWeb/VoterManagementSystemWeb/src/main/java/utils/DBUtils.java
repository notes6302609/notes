package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
private static Connection connection;
	
private static Connection openConnection() throws SQLException {
	String url="jdbc:mysql://localhost:3306/sunbeam22";
connection= DriverManager.getConnection(url ,"root","manager");
	return connection;
}
public static Connection getConnetion() throws SQLException
{
	return openConnection();
}
public static void closeConnection() throws SQLException
	{
		if(connection != null)
			connection.close();
	}
}
