package dao;

import java.sql.SQLException;

public interface IBankAccountDao {
	//function update_account_fn (sid  int ,did  int,amt double)  returns double
	String transferFunds_fn(int srcId,int destId,double transferAmount) throws SQLException;

}
