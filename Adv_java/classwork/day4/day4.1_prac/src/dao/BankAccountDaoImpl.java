package dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import utils.DBUtils;

public class BankAccountDaoImpl implements IBankAccountDao {
	private Connection conn;
	private CallableStatement cst;
	
	public BankAccountDaoImpl() throws SQLException {
	conn= DBUtils.openConnection();
	// func : "{?=call funcName(?,?,?,?....)}"
	cst= conn.prepareCall("{?=call update_account_fn(?,?,?)}");
	cst.registerOutParameter(1, Types.DOUBLE);
	System.out.println("account created");
	}
	@Override
	public String transferFunds_fn(int srcId, int destId, double transferAmount) throws SQLException {
		cst.setInt(2, srcId);
		cst.setInt(3, destId);
		cst.setDouble(4, transferAmount);
		cst.execute();
		
		return "Updated src balance "+cst.getDouble(1);
		
	}
	
	public void cleanUp() throws SQLException{
		if(cst!=null)
			cst.close();
		if(conn!=null)
			conn.close();
	}

}
