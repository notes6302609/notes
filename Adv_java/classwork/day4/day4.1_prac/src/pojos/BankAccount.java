package pojos;

//id | name | type | bal
public class BankAccount {
	private int acctNo;
	private String name;
	private String acType;
	private double balance;
	public BankAccount() {
		// TODO Auto-generated constructor stub
	}
	public int getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(int acctNo) {
		this.acctNo = acctNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAcType() {
		return acType;
	}
	public void setAcType(String acType) {
		this.acType = acType;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "BankAccount [acctNo=" + acctNo + ", name=" + name + ", acType=" + acType + ", balance=" + balance + "]";
	}
	
	
}
