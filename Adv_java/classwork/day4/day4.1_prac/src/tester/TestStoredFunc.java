package tester;

import java.util.Scanner;

import dao.BankAccountDaoImpl;

public class TestStoredFunc {

	public static void main(String[] args) {
		try(Scanner sc=new Scanner(System.in))
		{
			//create dao instance
			BankAccountDaoImpl acctDao=new BankAccountDaoImpl();
			System.out.println("Enter src a/c no , dest a/c no n transfer amount");
			System.out.println(acctDao.transferFunds_fn(sc.nextInt(), sc.nextInt(), sc.nextDouble()));
			acctDao.cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
