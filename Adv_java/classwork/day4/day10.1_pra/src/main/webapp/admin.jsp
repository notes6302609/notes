<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h2>hello,<h2>
	<h2> ${sessionScope.voter.voterDetails.name}</h2>
	<h5 style="color: green">${sessionScope.voter.message}</h5>

	<c:forEach var="t"
		items="${sessionScope.candidate.candidatesByVotes}">
		<h5>${t.name}: ${t.votes}</h5>
	</c:forEach>
</body>
</html>