<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<jsp:useBean id="voter" class="beans.VoterBean" scope="session"/>
<jsp:useBean id="candidate" class="beans.CandidateBean" scope="session"/>
<body>
<h5 align="center">Session ID : <%= session.getId() %></h5>
<h5 style="color: red">${sessionScope.voter.message}</h5>
<form action="validate.jsp" method="get">
      <table style="background-color: lightgrey; margin: auto">
        <tr>
          <td>Enter User name</td>
          <td><input type="text" name="name" /></td>
        </tr>
        <tr>
          <td>Enter Password</td>
          <td><input type="password" name="password" /></td>
        </tr>

        <tr>
          <td><input type="submit" value="Login" /></td>
        </tr>
      </table>
    </form>
</body>
</html>