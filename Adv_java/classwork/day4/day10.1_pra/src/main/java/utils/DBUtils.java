package utils;

import static utils.DBUtils.openConnection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtils {
	private static Connection connection;
	//open connection
	public static Connection openConnection(String dbURL,String userName,String password) throws /*ClassNotFoundException,*/ SQLException {
		//import driver
		//Class.forName("com.mysql.cj.jdbc.Driver");
		String url = "jdbc:mysql://localhost:3306/sunbeam22?useSSL=false&allowPublicKeyRetrieval=true";
		connection= DriverManager.getConnection(dbURL, userName, password);
	return connection;
	}
	//get connection
	public static Connection getConnection() {
		return connection;
	}
	
	public static void closeConnection() throws SQLException{
		if(connection != null)
			connection.close();
	}
	
	
}
