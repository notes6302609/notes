package beans;

import java.sql.SQLException;

import dao.VotersDaoImpel;
import pojos.Voters;

public class VoterBean {
// state (properties)
	private String name;
	private String password;
	private VotersDaoImpel voterDao;
	private Voters voterDetails;
	private String message;

	public VoterBean() throws SQLException {
		voterDao = new VotersDaoImpel();
		System.out.println("Voter bean created ...");

	}

	public Voters getVoterDetails() {
		return voterDetails;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public VotersDaoImpel getVoterDao() {
		return voterDao;
	}

	public void setVoterDao(VotersDaoImpel voterDao) {
		this.voterDao = voterDao;
	}

	public String getMessage() {
		return message;
	}

	public String validateVoter() throws SQLException {
		System.out.println("in validate Voter" + name + " " + password);
		// Voters authenticateUser(String name,String pwd ) throws SQLException;
		voterDetails = voterDao.authenticateUser(name, password);
		if (voterDetails != null) {
			message = "login Successfull";
			if (voterDetails.getRole().equalsIgnoreCase("admin"))
				return "admin";
			return "candidateList";
		}
		message = "invalid login, plz retry!!!!!!!!!!!!!";
		return "login";

	}

}
