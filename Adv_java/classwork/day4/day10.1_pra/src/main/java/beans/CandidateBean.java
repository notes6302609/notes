package beans;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

import dao.CandidateDaoImpl;
import pojos.Candidates;

public class CandidateBean {

	private CandidateDaoImpl candidateDao;

	public CandidateBean() throws SQLException {
		candidateDao = new CandidateDaoImpl();
	}

	public List<Candidates> getCandidatesByVotes() throws SQLException{
		return candidateDao.getCandidatesByVotes();
	}
	public LinkedHashMap<String, Integer> partywiseVotesAnalysys() throws SQLException{
		return candidateDao.partywiseVotesAnalysys();
	}
}
