package pages;

import static utils.DBUtils.closeConnection;
import static utils.DBUtils.openConnection;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;



/**
 * Servlet implementation class LoginServlet
 */

public class DBConnectionMgr extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	/**
	 * @see Servlet#init()
	 */
	@Override
	public void init() throws ServletException {
		ServletConfig config = getServletConfig();
		System.out.println("in init of " + getClass() + " servlet config " + config);// not null
		System.out.println("ctx "+getServletContext());//not null 
		// create user dao instance
		try {
			// open db connection
			openConnection(config.getInitParameter("url"), config.getInitParameter("user_name"),
					config.getInitParameter("password"));
		} catch (Exception e) {
			// ServletException(String mesg,Throwable rootCause)
			throw new ServletException("err in init of " + getClass(), e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
		
			closeConnection();
		} catch (Exception e) {
			// report the err to WC : RuntimeExc : OPTIONAL BUT rec.
			throw new RuntimeException("err in destroy of " + getClass(), e);
		}
	}

}
