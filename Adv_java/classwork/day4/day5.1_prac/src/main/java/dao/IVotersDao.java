package dao;

import java.sql.SQLException;

import pojos.Voters;

public interface IVotersDao {

	Voters authenticateUser(String name,String pwd ) throws SQLException;
	String updateVotingStatus(int voterId) throws SQLException;
}
