package pojos;

import java.time.LocalDate;
import javax.persistence.*;

/*
 * userId (PK) ,name,email,password,confirmPassword,role(enum), regAmount;
	 LocalDate/Date regDate;
	 byte[] image;
 */
@Entity // mandatory annotation to tell hibernate , following is an entity class , to be
		// managed by hibernate
@Table(name = "users_tbl")
public class User {
	@Id // mandatory to hib to add : PK constraint (can be applied field OR property
		// level : getter )
	// @GeneratedValue //for auto generation of PKs : def strategy : AUTO :
	// typically create hibernate_sequence --uses ids from this table
	@GeneratedValue(strategy = GenerationType.IDENTITY) // extra constraint : auto increment (well suited for mysql ) |
														// for oracle : seq generator
	@Column(name="user_id")
	private Long userId;
	@Column(length = 30) //varchar(30)
	private String name;
	@Column(length = 30,unique = true)//unique constraint
	private String email;
	@Column(length = 20,nullable = false)//NOT NULL constraint
	private String password;
	@Transient //skips from persistence ! => no column generated for this field.
	private String confirmPassword;
	@Enumerated(EnumType.STRING) //col type : varchar => names of constants
	@Column(length = 20,name="role")
	private Role userRole;
	@Column(name="reg_amount",nullable = false)
	private double regAmount;
	@Column(name="reg_date")
	private LocalDate regDate;
	@Lob //large object --> byte[] : type of blob : longblob(mysql)
	private byte[] image;

	public User() {
		System.out.println("in def ctor of " + getClass());
	}

	public User(String name, String email, String password, String confirmPassword, Role userRole, double regAmount,
			LocalDate regDate) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
		this.confirmPassword = confirmPassword;
		this.userRole = userRole;
		this.regAmount = regAmount;
		this.regDate = regDate;
	}

	// setter n getters
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public Role getUserRole() {
		return userRole;
	}

	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}

	public double getRegAmount() {
		return regAmount;
	}

	public void setRegAmount(double regAmount) {
		this.regAmount = regAmount;
	}

	public LocalDate getRegDate() {
		return regDate;
	}

	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", name=" + name + ", email=" + email + ", userRole=" + userRole
				+ ", regAmount=" + regAmount + ", regDate=" + regDate + "]";
	}

}
