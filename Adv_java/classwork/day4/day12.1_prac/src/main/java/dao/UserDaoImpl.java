package dao;

import pojos.Role;
import pojos.User;
import org.hibernate.*;
import static utils.HibernateUtils.getFactory;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		// user : NO(not a part L1 cache) , NO(no db rec) , exists in heap : TRANSIENT
		System.out.println("user id before " + user.getUserId());// null
		String mesg = "User registration Failed !!!!!!!!!!!";
		// get Session from SF
		Session session = getFactory().openSession(); // L1 cache is created
		Session session2 = getFactory().openSession(); // L1 cache is created
		System.out.println(session == session2);// false
		// begin tx
		Transaction tx = session.beginTransaction();
		System.out.println("is open " + session.isOpen() + " is connected " + session.isConnected());// t t
		try {
			System.out.println("Serializable unique id " + session.save(user));
			// user : PERSISTENT (i.e part of L1 cache)
			tx.commit();// session.flush() --> auto dirty checking ---> DML : insert --> to sync up
						// state of L1 cache to that of DB
			mesg = "User registered successfully " + user.getUserId();// not null
			System.out.println("is open " + session.isOpen() + " is connected " + session.isConnected());// t t
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			// re throw the exc to the caller
			throw e;
		} finally {
			if (session != null)
				session.close();// pooled out DB cn rets to the CP(conn pool) n L1 cache is destroyed
		}
		System.out.println("is open " + session.isOpen() + " is connected " + session.isConnected());// f f
		// user : DETACHED
		return mesg;
	}

	@Override
	public String registerUserWithCurrentSession(User user) {
		// user : NO(not a part L1 cache) , NO(no db rec) , exists in heap : TRANSIENT
		System.out.println("user id before " + user.getUserId());// null
		String mesg = "User registration Failed !!!!!!!!!!!";
		// get Session from SF
		Session session = getFactory().getCurrentSession(); // no existing session => new session created
		Session session2 = getFactory().getCurrentSession(); // existing session obj reted to the caller
		System.out.println(session == session2);// true
		// begin tx
		Transaction tx = session.beginTransaction();
		System.out.println("is open " + session.isOpen() + " is connected " + session.isConnected());// t t
		try {
			System.out.println("Serializable unique id " + session.save(user));
			// user : PERSISTENT (i.e part of L1 cache)
			tx.commit();// session.flush() --> auto dirty checking ---> DML : insert --> to sync up
						// state of L1 cache to that of DB
			mesg = "User registered successfully " + user.getUserId();// not null
			System.out.println("is open " + session.isOpen() + " is connected " + session.isConnected());// f f
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			System.out.println("is open " + session.isOpen() + " is connected " + session.isConnected());// f f
			// re throw the exc to the caller
			throw e;
		} // user : DETACHED
		return mesg;
	}

	@Override
	public User getUserDetailsById(long userId) {
		User user = null;// user : no state
		// get hibernate session from SF
		Session session = getFactory().getCurrentSession();
		// begin Tx
		Transaction tx = session.beginTransaction();
		try {
			user = session.get(User.class, userId);// in case of valid id , user : PERSISTENT
			user = session.get(User.class, userId);
			user = session.get(User.class, userId);
			user = session.get(User.class, userId);
			tx.commit();// using it here explicitly -- for closing session ---> L1 cache is destroyed n
						// db conn rets to poll
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			// re throw the exc to the caller
			throw e;
		}
		return user;// user : DETACHED
	}

	@Override
	public List<User> getAllUsers() {
		String jpql = "select u from User u";
		List<User> users = null;
		// get hibernate session from SF
		Session session = getFactory().getCurrentSession();
		// begin Tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).getResultList();
			// users : List of PERSISTENT entities
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			// re throw the exc to the caller
			throw e;
		}
		return users;// users : List of DETACHED entities
	}

	@Override
	public List<User> getSelectedUsers(LocalDate start, LocalDate end1, Role userRole) {
		List<User> users = null;
		String jpql = "select u from User u where u.regDate between :strt and :end and u.userRole=:rl";
		// get hibernate session from SF
		Session session = getFactory().getCurrentSession();
		// begin Tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("strt", start).setParameter("end", end1)
					.setParameter("rl", userRole).getResultList();
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			// re throw the exc to the caller
			throw e;
		}
		return users;
	}

	@Override
	public String getUserLogin(String email, String pwd) {

		String jpql = "select u from User u where u.email = :eml and u.password =:pwd";
		String mesg = "User Login Failed !!!!!!!!!!!";
		// get session from SF
		Session session = getFactory().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			User user = session.createQuery(jpql, User.class).setParameter("eml", email).setParameter("pwd", pwd)
					.getSingleResult();
			if(user != null)
				mesg="User Login successfull **************";
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return mesg;
	}

	@Override
	public List<String> getUserNamesByRegDate(LocalDate date) {
		List<String> names = null;
		String jpql ="Select u.name from User u where u.regDate > :rgd";
		
		Session session=getFactory().getCurrentSession();
		//begin tx
		Transaction tx=session.beginTransaction();
		try {
			names = session.createQuery(jpql,String.class).setParameter("rgd", date).getResultList();
			
			tx.commit();
		} catch (RuntimeException e) {
			if(tx != null)
				tx.rollback();
			throw e;
		}
		return names;
	}

	@Override
	public List<User> getSelectedDetailsByRegDate(LocalDate start, LocalDate end) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String changePassword(long userId, String newPassword) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String applyBulkDiscount(LocalDate date, double discount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteUserByEmail(String email) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String saveImage(long userId, String path) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String restoreImage(long userId, String path) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	

}
