package dao;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
//add a method for user registration(signup) : using openSession
	String registerUser(User transientUser);

	// add a method for user registration(signup) : using getCurrentSession
	String registerUserWithCurrentSession(User transientUser);

	// add a method to fetch user details by it's id
	User getUserDetailsById(long userId);

	// add a method to return list of all users
	List<User> getAllUsers();

	// add a method to display selected users
	List<User> getSelectedUsers(LocalDate start, LocalDate end, Role userRole);

	// add a method to verify login by username and passsword
	String getUserLogin(String email, String pwd);

	// add a method Display all user names registered after specific date
	List<String> getUserNamesByRegDate(LocalDate date);

	// add a method to fetch user name, reg amount , role who have registered
	// between specific dates
	List<User> getSelectedDetailsByRegDate(LocalDate start, LocalDate end);

	// add a method to change password of user
	String changePassword(long userId, String newPassword);

	// add a method for bulk updation
	String applyBulkDiscount(LocalDate date, double discount);

	// add a method to delete user details by it's email (single user delete)
	String deleteUserByEmail(String email);

	// add a method to save bin contents(eg : image) in the db
	String saveImage(long userId, String path) throws IOException;

	// add a method to restore bin contents(eg : image) from the db
	String restoreImage(long userId, String path) throws IOException;
}
