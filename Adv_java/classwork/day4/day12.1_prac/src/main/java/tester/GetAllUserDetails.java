package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;

public class GetAllUserDetails {

	public static void main(String[] args) {
		try (SessionFactory sf = getFactory()) {
			// create user dao instance
			UserDaoImpl userDao = new UserDaoImpl();// def ctor
			System.out.println("All Users ");
			userDao.getAllUsers().forEach(System.out::println);
			
		}//sc.close() , sf.close()
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
