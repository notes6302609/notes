package dao;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.List;

import pojos.Candidates;

public interface ICandidatesDao {
List<Candidates> getCandidateList() throws SQLException;
String incrementVotes(int candidateId) throws SQLException;
List<Candidates> getCandidatesByVotes() throws SQLException;
LinkedHashMap<String, Integer> partywiseVotesAnalysys() throws SQLException;
//List<Integer> partywiseVotesAnalysys() throws SQLException;
}
