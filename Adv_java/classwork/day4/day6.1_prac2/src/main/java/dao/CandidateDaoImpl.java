package dao;


import static utils.DBUtils.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;

import pojos.Candidates;
public class CandidateDaoImpl implements ICandidatesDao {
	private Connection cn;
	private PreparedStatement ps,ps1;
	private Statement st,st1;
	
	public CandidateDaoImpl() throws SQLException {
		cn = getConnection();
		st = cn.createStatement();
		String query2= "update candidates set votes= votes+1 where id=? ";
		ps= cn.prepareStatement(query2);
		
		st1= cn.createStatement();
		String query4 = "select party, votes from candidates group by party,votes";
		ps1=cn.prepareStatement(query4);
		System.out.println("candidate dao created");
	}

	@Override
	public List<Candidates> getCandidateList() throws SQLException {
		String query1="select * from candidates";
		try(ResultSet rs = st.executeQuery(query1)){
			//id | name    | party    | votes
			List<Candidates> candidateList = new ArrayList<>();
//			while(rs.next()) {
//				candidateList.add(new Candidates(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getInt(4)));
//			}
			while(rs.next()) {//| id | name    | party    | votes
				candidateList.add(new Candidates(rs.getInt("id"),rs.getString("name"),rs.getString("party"),rs.getInt("votes")));
			}
			return candidateList;
		}

	}

	@Override
	public String incrementVotes(int candidateId) throws SQLException {
		ps.setInt(1, candidateId);
		int rowsAffected = ps.executeUpdate();
		if(rowsAffected==1)
			return "successfully incremented votes";
		 return "increment failed";
	}
	


	

	@Override
	public LinkedHashMap<String, Integer> partywiseVotesAnalysys() throws SQLException
	{
		LinkedHashMap<String, Integer> lhm = new LinkedHashMap<>();
		
		try(ResultSet rs = ps1.executeQuery()){
			while(rs.next())
			{
				lhm.putIfAbsent(rs.getString(1), rs.getInt(2));
			}
		return lhm;
		}
	}
	
	
	@Override
	public List<Candidates> getCandidatesByVotes() throws SQLException {
		String query4="select * from candidates order by votes desc limit 2";
		try(ResultSet rs = st1.executeQuery(query4)){
			//id | name    | party    | votes
			List<Candidates> candidateList = new ArrayList<>();
		while(rs.next()) {//| id | name    | party    | votes
				candidateList.add(new Candidates(rs.getInt("id"),rs.getString("name"),rs.getString("party"),rs.getInt("votes")));
			}
		return candidateList;
		}
	}

	public void cleanUp() throws SQLException{
		if(ps!=null)
			ps.close();
		if(st!=null)
			st.close();
		if(st1!=null)
			st1.close();
		System.out.println("voters dao clean up");
		
	}


}
