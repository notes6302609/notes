package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import pojos.Candidates;
import pojos.Voters;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/admin")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()){
			HttpSession hs = request.getSession();
			System.out.println("in topic page is new=>"+hs.isNew());
			System.out.println("session id"+hs.getId());
			//get details of voter
			Voters user = (Voters) hs.getAttribute("user_Details");
			pw.print("<h1>Hello, Admin "+user.getName()+" </h1>");
			pw.print("<h3>top 2 candidate securing max vote</h3>");
			CandidateDaoImpl candiDao=(CandidateDaoImpl) hs.getAttribute("candidate_Dao");
			//System.out.println(candiDao);
			for(Candidates candi: candiDao.getCandidatesByVotes()) {
				pw.print(candi+"<br/>");
			}	
			pw.print("<h3>party wise votes analysis</h3>");
			Map<String, Integer> lhm = candiDao.partywiseVotesAnalysys();
		//	Set<String> lhmKeySet= lhm.keySet();
			for(String s : lhm.keySet()) {
			System.out.println(lhm.get(s));
				pw.print("<h5>Party: "+s+" votes: "+lhm.get(s)+"</h5>");
		}
			
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new ServletException("error in do-Post"+getClass(),e);
		}
	}

}
