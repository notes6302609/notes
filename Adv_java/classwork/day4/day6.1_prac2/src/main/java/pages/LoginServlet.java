package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.CandidateDaoImpl;
import dao.ICandidatesDao;
import dao.VotersDaoImpel;
import pojos.Voters;
import static utils.DBUtils.*;
@WebServlet(value="/authenticate", loadOnStartup=3)
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VotersDaoImpel voterDao;
	private CandidateDaoImpl candiDao;
	
	public void init(ServletConfig config) throws ServletException {
		try {
			openConnection();
			voterDao = new VotersDaoImpel();
			candiDao = new CandidateDaoImpl();
		}catch(Exception e) {
			throw new ServletException("err in init"+getClass(), e);
		}
	}

	/**
	 * @see Servlet#destroy()
	 */
	public void destroy() {
		try {
			
				candiDao.cleanUp();
				voterDao.cleanUp();
				closeConnection();
		}catch (Exception e) {
			throw new RuntimeException("err in destroy of " + getClass(), e);
			}
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//1.set content type
		response.setContentType("text/html");
		
		//2. get pw
		try (PrintWriter pw = response.getWriter())
		{
			//3.read req params
			String name = request.getParameter("name");
			String pass = request.getParameter("pass");
			//4.invoke dao's method for authentication
			Voters voter = voterDao.authenticateUser(name, pass);
			if(voter==null)//=>invalid login
				pw.print("<h3>Invalid username/password, please <a href='login.html'>Retry</a></h5>");
			else//=> in case of succesfull login
			{
				//pw.print("<h4>Login successfull<h4>");
				HttpSession session = request.getSession();
				System.out.println("from login servlet is session new =>"+session.isNew());
				System.out.println("Session ID "+session.getId());//server specific session id creation
				session.setAttribute("user_Details", voter);
				//add dao instances under session scope
				
				session.setAttribute("voter_Dao",voterDao);
				session.setAttribute("candidate_Dao",candiDao);
				
				if (voter.getRole().equalsIgnoreCase("voter")) 
					response.sendRedirect("voter");
				else
					response.sendRedirect("admin");
						
				
			}
		}
		catch (Exception e) {
				// TODO Auto-generated catch block
				throw new ServletException("error in do-Post"+getClass(),e);
			}
	
	}

}
