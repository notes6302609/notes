package pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Voters
 */
@WebServlet(value="/voter")
public class VoterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		try(PrintWriter pw = response.getWriter()){
			pw.print("<h5>voters page</h5>");
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				// cookies : available
				for (Cookie c : cookies)
					if (c.getName().equals("voter_details"))
						pw.print("<h5> voter Details retrieved from a Cookie " +c.getValue()+ "</h5>");
			} else
				pw.print("<h5> No Cookies !!!!!!!!!!!!! Session Management Failed !!!!!!!!!!!!!!!!!!!!!</h5>");

		}
	}

}
