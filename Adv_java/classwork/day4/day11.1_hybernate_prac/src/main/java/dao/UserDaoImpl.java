package dao;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pojos.Role;
import pojos.User;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User transientUser) {
		String mesg = "User registration Failed !!!!!!!!!!!";
		Session session = getFactory().openSession();
		System.out.println(session);
		Transaction tx = session.beginTransaction();
		try {
			System.out.println("Serializable id" + session.save(transientUser));
			tx.commit();
			mesg = "User registered successfully " + transientUser.getUserId();// not null
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;

		} finally {
			if (session != null)
				session.close();
		}
		System.out.println(session + " is open" + session.isOpen());

		return mesg;
	}

	@Override
	public String registerUserWithCurrentSession(User transientUser) {
		Session session = getFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return null;
	}

	@Override
	public User getUserDetailsById(long userId) {
		User user = null;

		Session session = getFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			user = session.get(User.class, userId);
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return user;
	}

	@Override
	public List<User> getAllUsers() {
		String jpql = "select u from User u";
		List<User> users = null;
		Session session = getFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).getResultList();
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return users;
	}

//	@Override
//	public List<User> getSelectedUsers(LocalDate start, LocalDate end1, Role userRole) {
//		List<User> users = null;
//		// String jpql = "select u from User where u.regDate between :strt and :end and
//		// u.userRole=:rl";
//		String jpql="select u from User u where u.regDate between :strt and :end and u.userRole=:rl";
//		Session session = getFactory().getCurrentSession();
//		Transaction tx = session.beginTransaction();
//		try {
//
//			users=session.createQuery(jpql, User.class)
//					.setParameter("strt",start)
//					.setParameter("end", end1)
//					.setParameter("rl", userRole)
//					.getResultList();
//		
//			tx.commit();
//		} catch (RuntimeException e) {
//			if (tx != null)
//				tx.rollback();
//			throw e;
//		}
//		return users;
//	}

	@Override
	public List<User> getSelectedUsers(LocalDate start, LocalDate end1, Role userRole) {
		List<User> users = null;
	
		String jpql="select u from User u where u.regDate between ?1 and ?2 and u.userRole=?3";
		Session session = getFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		try {

			users=session.createQuery(jpql, User.class)
					.setParameter(1,start)
					.setParameter(2, end1)
					.setParameter(3, userRole)
					.getResultList();
		
			tx.commit();
		} catch (RuntimeException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		return users;
	}
}
