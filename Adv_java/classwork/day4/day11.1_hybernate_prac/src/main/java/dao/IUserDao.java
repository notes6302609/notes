package dao;

import java.time.LocalDate;
import java.util.List;

import pojos.Role;
import pojos.User;

public interface IUserDao {
	String registerUser(User transientUser);
	// add a method for user registration(signup) : using getCurrentSession
		String registerUserWithCurrentSession(User transientUser);
		//add a method to fetch user details by it's id
		User getUserDetailsById(long userId);
		//add a method to return list of all users
		List<User> getAllUsers();
		//add a method to display selected users
		List<User> getSelectedUsers(LocalDate start,LocalDate end,Role userRole);
}
