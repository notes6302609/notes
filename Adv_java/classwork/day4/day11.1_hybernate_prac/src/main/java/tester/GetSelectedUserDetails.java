package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;
import static java.time.LocalDate.parse;
import static pojos.Role.valueOf;

public class GetSelectedUserDetails {

	public static void main(String[] args) {
		try (SessionFactory sf = getFactory();Scanner sc=new Scanner(System.in)) {
			// create user dao instance
			UserDaoImpl userDao = new UserDaoImpl();// def ctor
			System.out.println("Enter begin date , end date n role");
			
			userDao.getSelectedUsers(parse(sc.next()), parse(sc.next()), valueOf(sc.next().toUpperCase()))
			.forEach(System.out::println);
			
		}//sc.close() , sf.close()
		catch (Exception e) {
			e.printStackTrace();
		}

	}

}
