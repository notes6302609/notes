package tester;

import static utils.HibernateUtils.getFactory;

import java.time.LocalDate;
import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.UserDaoImpl;
import dao.UserDaoImpl;
import pojos.Role;
import pojos.User;
//saurabh s@m 1234 1234 customer 32827 2022-05-12
public class UserRegistration {

	public static void main(String[] args) {
		try (SessionFactory sf = getFactory(); Scanner sc = new Scanner(System.in)) {
			// create user dao instance
			UserDaoImpl userDao = new UserDaoImpl();// def ctor
			System.out.println(
					"Enter user details name,  email,  password,  confirmPassword,  userRole,  regAmount regDate(yr-mon-day)");
			System.out.println(userDao.registerUser(new User(sc.next(), sc.next(), sc.next(), sc.next(),
					Role.valueOf(sc.next().toUpperCase()), sc.nextDouble(), LocalDate.parse(sc.next()))));

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
