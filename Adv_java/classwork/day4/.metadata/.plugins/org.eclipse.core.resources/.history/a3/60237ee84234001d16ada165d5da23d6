package com.app.service;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.customExceptions.ResourceNotFoundException;
import com.app.dao.BillRepository;
import com.app.dao.EmployeeRespository;
import com.app.dao.PathologyRepository;
import com.app.dao.PrescriptionRepository;
import com.app.dao.TreatmentRepository;
import com.app.dto.EmployeeModifyDetailsDto;
import com.app.dto.EmployeeResponseDto;
import com.app.dto.TreatmentBasicDetailsResponseDto;
import com.app.dto.TreatmentDetailedResponseDto;
import com.app.dto.TreatmentPatientDetailDto;
import com.app.dto.UpdatableReportRequestDto;
import com.app.entities.Bill;
import com.app.entities.Employee;
import com.app.entities.Pathology;
import com.app.entities.Patient;
import com.app.entities.Prescription;
import com.app.entities.Role;
import com.app.entities.Treatment;

@Service
@Transactional
public class DoctorServiceImpl implements IDoctorService {
	
	
	@Autowired
	private PrescriptionRepository presRepo;
	
	@Autowired
	private ModelMapper mapper;
	
	@Autowired
	private PathologyRepository pathoRepo;
		
	@Autowired
	private EmployeeRespository empRepo;
	
	@Autowired
	private TreatmentRepository treatmentRepo;
	
	@Autowired
	private BillRepository billRepo;

	@Override
	public Employee findMyDetails(Long empId) {
		return empRepo.findById(empId).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ empId)
				);
	}

	@Override
	public String updateMyDetails(EmployeeModifyDetailsDto myDetails, Long empId) {
		
		Employee verifiedDoctor =  findMyDetails(empId);
		if(verifiedDoctor != null)
		{
			if(verifiedDoctor.getRole() == Role.DOCTOR)
			{
				mapper.map(myDetails, verifiedDoctor);
				empRepo.save(verifiedDoctor);
				return "Employee Details modified successfully!!!!";
				
			}
			else return "Employee is not a doctor!!!";
		}
		else return "No Such Employee!!!!";
	}
	
	@Override
	public List<TreatmentBasicDetailsResponseDto> findAllUpcomingAppointments(Long empId) {
		
		List<TreatmentBasicDetailsResponseDto> tdto = new ArrayList<TreatmentBasicDetailsResponseDto>();
//		System.out.println("inside get details");
		
		List<Treatment> treatmentList = treatmentRepo.findByEmployeeAndPendingTreatments(empId).orElseThrow(
				() -> new ResourceNotFoundException("Treatments does not exist with Id: "+ empId)
				);
		
//		for(Treatment trt : treatmentList)
//			System.out.println(trt.toString());
	
		for( int i=0; i< treatmentList.size();i++)
		{
			TreatmentPatientDetailDto ptnDtls = new TreatmentPatientDetailDto();
			Patient ptn = treatmentList.get(i).getReportNumber().getPatient();
			mapper.map(ptn, ptnDtls);
			
			TreatmentBasicDetailsResponseDto trtmntDtls = new TreatmentBasicDetailsResponseDto();
			trtmntDtls.setPatient_details(ptnDtls);
			
			
			trtmntDtls.setDateOfAppointment(treatmentList.get(i).getDateOfAppointment());
			trtmntDtls.setTreatmentSlot(treatmentList.get(i).getTreatmentSlot());
			trtmntDtls.setPatientNote(treatmentList.get(i).getPatientNote());
			trtmntDtls.setPatientProblem(treatmentList.get(i).getPatientProblem());
			trtmntDtls.setReportNumberToSend(treatmentList.get(i).getReportNumber().getBillNumber());
					
			tdto.add(trtmntDtls);
		}
		return tdto;
	}

	@Override
	public TreatmentDetailedResponseDto getAppointmentByReportNumber(Long reportNumber) {
		
//		System.out.println("inside doctor service");
		Treatment treatmentFound = treatmentRepo.findByPatientReportNumber(reportNumber).orElseThrow(
				() -> new ResourceNotFoundException("Treatment does not exist with report number: "+ reportNumber)
				);
		
//		System.out.println(treatmentFound.toString());
		Patient patient = billRepo.getPatientByreportNumber(reportNumber).orElseThrow(
				() -> new ResourceNotFoundException("Bill does not exist with report number: "+ reportNumber)
				) ;
//		System.out.println(patient.toString());
		TreatmentPatientDetailDto pdto =  new TreatmentPatientDetailDto();
		TreatmentDetailedResponseDto dto = new TreatmentDetailedResponseDto();
		
//		System.out.println(dto.toString());
//		System.out.println(pdto.toString());
		if(treatmentFound.getBed() != null)
		mapper.map(treatmentFound.getBed(), dto);
		mapper.map(patient, pdto);
		dto.setPatient_details(pdto);
//		System.out.println( "patient dto after mapping: " + pdto.toString());
//		dto.setBedNumber(treatmentFound.getBed().getBedNumber());
//		dto.setWardNumber(treatmentFound.getBed().getWardNumber());
		mapper.map(treatmentFound, dto);
//		System.out.println("treatment dto after mapping: " +dto.toString());
		return dto;
	}

	@Override
	public String updateAppointmentDetailsByReportNumber(Long reportNumber, UpdatableReportRequestDto dto) {
		//set medicines list
		//set test names
		
		Treatment treatmentToUpdate = treatmentRepo.findByPatientReportNumber(reportNumber).orElseThrow(
				() -> new ResourceNotFoundException("Treatment does not exist with report number: "+ reportNumber)
				);
		if(treatmentToUpdate != null)
		{
		mapper.map(dto, treatmentToUpdate);
		
		Treatment t = treatmentRepo.save(treatmentToUpdate);
		if(t!=null) {
//		System.out.println("1/////////////////////"+t.toString());
		
		Boolean verifyPath = pathoRepo.findByPatientReportNumber(reportNumber).isPresent();
//		System.out.println("2/////////////////////"+verifyPath);

		if(!verifyPath){
//			System.out.println("hii");
			Pathology pathToEnter = new Pathology();
//			System.out.println("hii again"+dto.getTest_details().getTestName());
		pathToEnter.setTestName(dto.getTest_details().getTestName());
		Bill b = billRepo.findByBillNumber(reportNumber).orElseThrow(
				() -> new ResourceNotFoundException("Bill does not exist with report number: "+ reportNumber)
				);
		pathToEnter.setReportNumber(b.getTreatment().getReportNumber());
//		System.out.println("path to enter"+pathToEnter);
		pathoRepo.save(pathToEnter);
		}
		
		List<Prescription> verifyPres = presRepo.getPrescriptionDetailsByReportNumber(reportNumber).orElseThrow(
				() -> new ResourceNotFoundException("Prescriptions does not exist with report Number: "+ reportNumber)
				);
//		System.out.println("12/////////////////////"+verifyPres);

		if(verifyPres.size() == 0){
//			System.out.println("hii");
		for(int i=0;i<dto.getMedicines_list().size();i++)
		{
			Prescription presToEnter = new Prescription();
//			System.out.println("hii again"+dto.getMedicines_list().get(i));
			mapper.map(dto.getMedicines_list().get(i), presToEnter);
			
			Bill b = billRepo.findByBillNumber(reportNumber).orElseThrow(
					() -> new ResourceNotFoundException("Bill does not exist with report number: "+ reportNumber)
					);
			presToEnter.setReportNumber(b.getTreatment().getReportNumber());
//		System.out.println("path to enter"+presToEnter);
		presRepo.save(presToEnter);
		}
		}
		if(t !=null)
			return "Report Updated Successfully!!";

		}
		return "failed to update report Details";
		}
		else
			return "Treatment is not present";
	}

	
	
	
	@Override
	public EmployeeResponseDto getMyDetails(Long empId) {
		Employee emp = empRepo.findById(empId).orElseThrow(() -> new ResourceNotFoundException("Invalid Emp Id"));
		return mapper.map(emp, EmployeeResponseDto.class);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
