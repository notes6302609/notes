package com.app.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.customExceptions.ResourceNotFoundException;
import com.app.dao.BedRepository;
import com.app.dao.EmployeeRespository;
import com.app.dao.WebsiteStaticDataRepository;
import com.app.entities.Bed;
import com.app.entities.Employee;
import com.app.entities.Role;
import com.app.entities.WebsiteStaticData;

@Service
@Transactional
public class AdminServiceImpl implements IAdminService {

	@Autowired
	private EmployeeRespository employeeRespository;
	
	@Autowired
	private BedRepository bedRepository;
	
	@Autowired
	private WebsiteStaticDataRepository wsdRepo;
	
	@Override
	public Employee AddDoctor(Employee doctor) {
		
		if(doctor.getRole() != Role.ROLE_DOCTOR)
		{
			doctor.setRole(Role.ROLE_DOCTOR);
		}
		
		Employee newDoctor = employeeRespository.save(doctor);
		return newDoctor;		
	}

	@Override
	public boolean DeleteDoctorById(Long id) {
		if(id <= 0)
		{
			return false;
		}
		
		Employee extemp = employeeRespository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ id)
				);
		if(extemp != null && extemp.getRole()== Role.DOCTOR )
		{
			employeeRespository.deleteById(id);
			return true;
		}
	
		return false;
	}

	@Override
	public boolean UpdateDoctor(Employee emp) {
		
		Long id = emp.getEmpId();
		
		Employee extemp = employeeRespository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ id)
				);
		
		if(extemp != null && extemp.getRole()== Role.ROLE_DOCTOR)
		{
			employeeRespository.save(emp);
			return true;
		}
		return false;
			
	}

	@Override
	public Employee AddPathologist(Employee pathologist) {

		if(pathologist.getRole() != Role.ROLE_DOCTOR)
		{
			pathologist.setRole(Role.ROLE_PATHOLOGIST);
		}
		
		Employee newpathologist = employeeRespository.save(pathologist);
		return newpathologist;
	}

	@Override
	public boolean DeletePathologist(Long id) {
		if(id <= 0)
		{
			return false;
		}
		
		Employee extemp = employeeRespository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ id)
				);
		if(extemp != null && extemp.getRole()== Role.ROLE_PATHOLOGIST )
		{
			employeeRespository.deleteById(id);
			return true;
		}
	
		return false;
	}

	@Override
	public boolean UpdatePathologist(Employee emp) {
        
		Long id = emp.getEmpId();
		
		Employee extemp = employeeRespository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ id)
				);
		
		if(extemp != null && extemp.getRole()== Role.PATHOLOGIST)
		{
			employeeRespository.save(emp);
			return true;
		}
		return false;
	}

	@Override
	public Employee AddReceptionist(Employee receptionist) {
		
		if(receptionist.getRole()== null)
		{
			receptionist.setRole(Role.RECEPTIONIST);
		}
		
		Employee newReceptionist = employeeRespository.save(receptionist);
		
		return newReceptionist;
	}

	@Override
	public boolean DeleteReceptionist(Long id) {
		
		if(id <= 0)
		{
			return false;
		}
		
		Employee emp = employeeRespository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ id)
				);
		
		if(emp != null && emp.getRole()== Role.RECEPTIONIST)
		{
			employeeRespository.deleteById(id);
			return true ;
		}
		
		return false;
	}

	@Override
	public boolean UpdateReceptionist(Employee receptionist) {
		
        Long id = receptionist.getEmpId();
		
		Employee extemp = employeeRespository.findById(id).orElseThrow(
				() -> new ResourceNotFoundException("Employee does not exist with Id: "+ id)
				);
		if(extemp != null && extemp.getRole()== Role.RECEPTIONIST)
		{
			employeeRespository.save(receptionist);
			return true ;
		}
		else
			return false;
		
	}

	@Override
	public Long AddBeds(Bed bed) {
		
		Bed bedAdded = bedRepository.save(bed);
		
		return bedAdded.getBedNumber();
	}
	
	@Override
	public WebsiteStaticData updateWebsiteStaticData(WebsiteStaticData wsd) {
		return wsdRepo.save(wsd);
	}

	@Override
	public WebsiteStaticData getWebsiteStaticData() {
		return wsdRepo.findById(1L).orElseThrow(
				() -> new ResourceNotFoundException("Not content to display!!!"));
	}
	
	

}
