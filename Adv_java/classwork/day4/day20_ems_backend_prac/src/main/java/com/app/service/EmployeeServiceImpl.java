package com.app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.config.ConfigDataResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.custom_exception.ResourceNotFoundException;
import com.app.dao.EmployeeRepository;
import com.app.entities.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements IEmployeeService {

	@Autowired
	private EmployeeRepository empRepo;

	@Override
	public List<Employee> getAllEmployees() {
		// TODO Auto-generated method stub
		return empRepo.findAll();
	}

	@Override
	public Employee insertEmpDetails(Employee transientEmp) {
		// TODO Auto-generated method stub
		return empRepo.save(transientEmp);
	}

	@Override
	public String deleteEmpDetails(long empId) {
		String mesg = " Deleting emp details faied!!!!!!";
		if (empRepo.existsById(empId)) {
			empRepo.deleteById(empId);
			mesg = "Deleted emp details of emp of" + empId;
		}
		return mesg;
	}

	@Override
	public Employee getEmpDetails(long empId) {

		return empRepo.findById(empId).orElseThrow(() -> new ResourceNotFoundException("Invalid Emp ID!!!!!!"));
	}

	@Override
	public Employee updateEmpDetails(Employee detachedEmp) {
		empRepo.findById(detachedEmp.getId()).orElseThrow(() -> new ResourceNotFoundException("Invalid Emp ID!!!!!!"));
		return empRepo.save(detachedEmp);
	}

}
