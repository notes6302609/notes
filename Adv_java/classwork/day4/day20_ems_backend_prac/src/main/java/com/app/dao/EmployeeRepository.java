package com.app.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.entities.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	List<Employee> findByDepartmentNameOrderBySalaryDesc(String deptName);
//findByJoinDateBetween(LocalDate start, LocalDate end
	// findByStartDateBetween

	List<Employee> findByJoinDateBetween(LocalDate start, LocalDate end);
	// update sal by department : sal incr;
	@Modifying
	@Query("update Employee e set e.salary= e.salary + ?1 where e.departmentName=?2")
	int updateEmpSalary(double salIncrement, String dept);

	// finder for login
	Optional<Employee> findbyEmailAndPassword(String em, String pass);

}
