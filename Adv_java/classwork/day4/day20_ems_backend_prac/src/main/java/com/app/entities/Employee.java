package com.app.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
@Entity
@Table(name = "emp_tbl", uniqueConstraints = @UniqueConstraint(columnNames = { "first_name", "last_name" }))

public class Employee extends BaseEntity {
	@Column(length = 30, name = "first_name")
	private String firstName;

	@Column(length = 30, name = "last_name")
	private String lastName;
	@Column(length = 30, unique = true)
	private String email;
	@Column(length = 30)
	private String password;
	@Column(length = 30)
	private String location;
	@Column(length = 40)
	private String departmentName;
	private double salary;
	private LocalDate joinDate;

}
