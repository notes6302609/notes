package com.app.service;

import java.util.List;

import com.app.entities.Employee;

public interface IEmployeeService {
	List<Employee> getAllEmployees();

	Employee insertEmpDetails(Employee transientEmp);

	String deleteEmpDetails(long empId);

	Employee getEmpDetails(long empId);

	Employee updateEmpDetails(Employee detachedEmp);
	// add auth method
	// EmployeeDTO authenticateEmployee(LoginRequestDTO request);
}
