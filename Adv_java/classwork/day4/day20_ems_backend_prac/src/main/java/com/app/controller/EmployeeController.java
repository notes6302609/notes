package com.app.controller;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.entities.Employee;
import com.app.service.IEmployeeService;

@RestController
@RequestMapping("/api/employees")
@CrossOrigin(origins = "http://localhost:3000")
@Validated
public class EmployeeController {

	@Autowired
	private IEmployeeService empService;

	public EmployeeController() {
		System.out.println("in ctor of " + getClass());
	}

	@GetMapping
	public List<Employee> fetchAllEmpDetails() {
		System.out.println("in fetch all");
		return empService.getAllEmployees();

	}

	@PostMapping
	public ResponseEntity<?> addNewEmp(@RequestBody Employee emp) {
		System.out.println("in add new emp" + emp.getId());
		return ResponseEntity.status(HttpStatus.CREATED).body(empService.insertEmpDetails(emp));

	}

	@DeleteMapping("/{eid}")
	public String deleteEmpDetails(@PathVariable @Valid @NotNull @Range(min = 1, max = 100) long eid) {
		System.out.println("in del emp dtls " + eid);
		return empService.deleteEmpDetails(eid);
	}

	@GetMapping("/{empId}")
	public ResponseEntity<?> getEmpDetails(
			@PathVariable @Valid @Range(min = 1, max = 100, message = "emp id must be within 1-100") long empId) {
		return new ResponseEntity<>(empService.getEmpDetails(empId), HttpStatus.OK);

	}

	@PutMapping
	public Employee updateEmpDetails(@RequestBody Employee emp) {
		System.out.println("in update  emp dtls" + emp);// not null id
		return empService.updateEmpDetails(emp);
	}

}
