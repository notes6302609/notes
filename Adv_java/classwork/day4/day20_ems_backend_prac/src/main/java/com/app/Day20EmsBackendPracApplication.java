package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Day20EmsBackendPracApplication {

	public static void main(String[] args) {
		SpringApplication.run(Day20EmsBackendPracApplication.class, args);
	}

}
