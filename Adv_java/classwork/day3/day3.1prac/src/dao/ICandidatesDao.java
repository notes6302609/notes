package dao;

import java.sql.SQLException;
import java.util.List;

import pojos.Candidates;

public interface ICandidatesDao {
List<Candidates> getCandidateList() throws SQLException;
String incrementVotes(int candidateId) throws SQLException;
}
