package dao;

import java.sql.*;

import pojos.Voters;


import static utils.DBUtils.*;

public class VotersDaoImpel implements IVotersDao {
	
	private Connection cn;
	private PreparedStatement ps1,ps2;
	
	
	public VotersDaoImpel() throws SQLException {
		cn= openConnection();
		String query1 ="select * from voters where name=? and password =? ";
		 ps1 = cn.prepareStatement(query1);
		String query2 ="update voters set status=1 where id =?";
		ps2 = cn.prepareStatement(query2);
		
		System.out.println("voters Daoimpel Created");
	}


	public Voters authenticateUser(String name, String pwd) throws SQLException {
		ps1.setString(1, name);
		ps1.setString(2,pwd);
		try(ResultSet rs1 = ps1.executeQuery()){
			if(rs1.next())
			{	
				// id | name    | email             | password | status | role
				return new Voters( rs1.getInt(1),rs1.getString(2),rs1.getString(3),rs1.getString(4),rs1.getBoolean(5),rs1.getString(6));
			}
			
		}
		return null;
	}

	@Override
	public String updateVotingStatus(int id) throws SQLException {
		ps2.setInt(1,id);
		 int rowsAffected = ps2.executeUpdate();
		if(rowsAffected==1)
			return "successfully voted";
		 return "voting failed";
	}
	
	public void cleanUp() throws SQLException {
		if (ps1 != null)
			ps1.close();
		if (ps2 != null)
			ps2.close();
		
		closeConnection();

	}

}
