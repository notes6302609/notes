package com.app;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.app.entities.CountryEntities;
import com.app.repository.CountryRepository;
@SpringBootTest
class TestCountryRepository {
	@Autowired
	private CountryRepository countryRepo;
	
	@Test
	void test() {
		List<CountryEntities> list= countryRepo.sortAllCountryByPopulation();
		list.forEach(System.out::println);
		assertEquals(5, list.size());
		
	
	}

}
