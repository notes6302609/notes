package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name="country")
public class CountryEntities {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int id;
@Column(length=30)
private String countryName;
@Column(length=30)
private String continent;
private double population;

}
