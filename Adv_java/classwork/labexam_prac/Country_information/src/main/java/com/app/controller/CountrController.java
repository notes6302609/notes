package com.app.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.repository.CountryRepository;

@Controller
@RequestMapping("/country")
public class CountrController {
@Autowired
private CountryRepository countrepo;
	
	@GetMapping("/sort_by_population")
	public String sortByPopulation(Model map) {
		System.out.println("inside sort by population");
		map.addAttribute("country_list", countrepo.sortAllCountryByPopulation());
		return "/country/sort_by_population";
		
	}
}
