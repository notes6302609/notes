package com.app.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.app.entities.CountryEntities;

@Transactional
public interface CountryRepository extends JpaRepository<CountryEntities, Integer> {

@Query("select c from CountryEntities c order by c.population")
	public List<CountryEntities> sortAllCountryByPopulation();
	
}
