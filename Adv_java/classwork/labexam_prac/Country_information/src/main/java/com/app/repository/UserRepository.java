package com.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.entities.UserEntities;

public interface UserRepository extends JpaRepository<UserEntities, Integer> {
	Optional<UserEntities> findByEmailAndPassword(String em, String pass);
	
}
