package com.app.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString(exclude ="password")
@NoArgsConstructor
@AllArgsConstructor
@Table(name="user")
public class UserEntities {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private int empid;
@Column(length=30)
private String name ="saurabh";
@Column(length=30, unique =true)
private String email;
@Column(length=30)
private String password;
}
