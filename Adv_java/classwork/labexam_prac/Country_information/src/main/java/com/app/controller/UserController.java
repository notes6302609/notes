package com.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.entities.UserEntities;
import com.app.repository.UserRepository;

@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserRepository userRepo;

	@GetMapping("/signup")
	public String signup(UserEntities user) {
		System.out.println("inside get signup");
		return "/user/signup";
	}
//
	@PostMapping("/signup")
	public String signup1( UserEntities user) {

		System.out.println("inside post signup");
		userRepo.save(user);
		return "/index";
	}
}
