#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(void)
{
    int fw;
    fw = open("dac_fifo",O_WRONLY);

    if (fw < 0)
    {
        perror("open failed !!!");
        _exit(1);
    }

    write(fw, "PIPE MECHANISUM transfer", 15);

    close(fw);
    return 0;
}
