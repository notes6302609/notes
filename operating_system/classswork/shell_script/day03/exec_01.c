#include<stdio.h>
#include<unistd.h>
#include <sys/wait.h>


int main(void)
{
    int ret;
    int i=0;
        int status;
			    int ret_exec;

				printf("main() started!!!");

    ret = fork();
    if( ret == -1)
    {
        perror("fork() sys call failed!!!\n");
        _exit(1);
    }

    if( ret == 0 )
    {
        ret_exec = execl("/home/osboxes/saurabh/shell_script/day03/cmdlinedemo.out", "cmdlinedemo.out", "sachin", "rahul", "rohit", NULL);
    
        if(ret_exec == -1)
        {
            perror("execl() failed !!!\n");

            _exit(1);
        }      
    else
     {
        wait(&status);
        printf("exit value of child process is : %d\n", WEXITSTATUS(status));
    }
  

    return 0;
    //$ps -t pts/0 -m -o pid,ppid,cmd
}
}
