#!/bin/bash
clear

function print_table( )
{
				echo "table of $1 is : "
								for i in {1..10..1}
								do
													res=`expr $1 \* $i`
													echo "$res"
								done
}

clear

echo "scrip $0  tstarted!!!!"

#print_table $1

#table=$( print_table $1 )
echo -n "enter the number : "
read num

# way-1 function calling
# print_table $num

# print_table $1	# $1 => pos param

# way-2 function calling => function returns value and we are catching it int a variable named as "table"
table=$( print_table $num )

echo "table function way 2 : $table"
echo "$0 sceipt ended"
