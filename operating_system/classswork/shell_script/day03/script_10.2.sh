#!/bin/bash

# script to print a pattern


clear

# accept no. of rows from user
echo -n "enter n : "
read n

i=0
while [ $i -lt $n ]	# outer while loop - rows
do

	j=0
	while [ $j -lt `expr $n - $i` ]	# inner while loop - columns
	do
		echo -n " * "
		j=`expr $j + 1`	# j++
	done

	echo " "	# printf("\n");
	i=`expr $i + 1`	# i++
done

exit



#for( i = 0 ; i < n ; i++ )
#{
#		for( j = 0 ; j < n-i ; j++ )
#		{
#			printf("%4c", '*');
#		}
#
#		printf("\n");
#}



