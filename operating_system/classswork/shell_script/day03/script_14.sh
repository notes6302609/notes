#!/bin/bash

function add( )
{
				res=`expr $1 + $2`
								echo "$res"
}
function sub( )
{
				res=`expr $1 - $2`
								echo "$res"
}
function multi( )
{
				res=`expr $1 \* $2`
								echo "$res"
}
function div( )
{
				res=`expr $1 / $2`
								echo "$res"
}

function menu( )
{
		clear

		# display menu list
		echo "***** calculator *****"
		echo "0. exit"
		echo "1. addition"
		echo "2. substraction"
		echo "3. multiplication"
		echo "4. division"

		echo -n "enter the choice : "
		read choice
}

echo " script $0 started..."


while [ true ]
do
	clear
	menu

	if [ $choice -eq 0 ]
	then
		exit
	fi

	echo -n "enter the value of num1: "
	read num1

	echo -n "enter the value of num2: "
	read num2

	case $choice in
	1)
			res=$(add $num1 $num2)
			echo "result : $res"
				;;
	2)
			res=$(sub $num1 $num2)
			echo "result : $res"
				;;
	3)
			res=$(multi $num1 $num2)
			echo "result : $res"
				;;
	4)
			res=$(div $num1 $num2)
			echo "result : $res"
				;;
	*) 
			echo "invalid choice!!"
			;;
		esac
		echo "press enter to continue"
			read enter
done
