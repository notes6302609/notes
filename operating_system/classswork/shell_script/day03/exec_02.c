#include<stdio.h>
#include<unistd.h>
#include<sys/wait.h>

int main(void)
{
    int ret;
    int i;
    int ret_exec;
    int status;

    ret = fork();

    if( ret == -1 )
    {
        perror("fork() sys call failed !!!\n");
        _exit(1);
    }

    if( ret == 0 )//child process
    {
        char *args[] = { "ls", "-l", "-i", "-s", NULL };

       // ret_exec = execl("/bin/ls", "ls", "-l", "-i", "-s", NULL);
        
       ret_exec = execlp("ls", "ls", "-l", "-i", "-s", NULL);

     //   ret_exec = execv("/bin/ls", args);


        if( ret_exec == -1 )
        {
            perror("exec() failed !!!\n");
            /*
                perror() is C lib function which displays "actual system error message", with user error message
            */
           _exit(1);
        }

       //logic => replaced by new process
    }
    else//parent process
    {
        wait(&status);
        printf("exit value of child process is : %d\n", WEXITSTATUS(status));
    }

    return 0;
}
