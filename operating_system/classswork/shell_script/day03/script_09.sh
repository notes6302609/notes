#!/bin/bash

# script :
# to accept filepath from user
# if filepath is invalid then print message "invalid filepath"
# if filepath is valid:
	# if filepath is a regular file => display its contents (by using cat command)
	# else if filepath is a directory file => display its contents (by using ls command)
	# else print message "filepath is neither regular file nor directory file"


clear

# accept filepath from user
echo -n "enter filepath : "
read filepath

if [ -e $filepath ]	# if filepath is valid
then	
	
	echo "filepath is valid"
	if [ -f $filepath ] # if filepath is a regular file
	then
		echo "$filepath is a regular file, and its contents are : "
		cat $filepath
	elif [ -d $filepath ] # if filepath is a directory
	then 
			echo "$filepath is directory file and its content are : "
			ls -l $filepath
	else
			echo "$filepath is neighther a regular file nor a directory"
	fi

else
	echo "invalid filepath !!!"
fi

exit

