#!/bin/bash

# the above line is referred as a "shebang line", which contains #! followed by
# an absolute path of shell program which will going to execute the script
# which is not mandatory if curently active shell is "bash"
# if cur active shell is not bash and we written script for bash, then it is compulsory
# to mention shebang line.
# it is always good practice to mention shebang line



# script to demonstrate set of basic linux commands

# in shell script programming language whatever is there till end of the line in front
# of "#" is considered as a comment.


clear

echo -n "today's date is : "

date +"%d / %m / %Y"

echo "cal of cur month is : "

cal

# the above line is referred as a "shebang line", which contains #! followed by
# which is not mandatory if curently active shell is "bash"
# an absolute path of shell program which will going to execute the script
exit # exit the script
