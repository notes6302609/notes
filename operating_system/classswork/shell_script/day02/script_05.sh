#!/bin/bash

clear

echo -n "enter the number: "
read num

i=1

echo "while => table of $num is : "

while [ $i -le 10 ]
do 
		res=`expr $num \* $i`
		echo "$res"
		i=`expr $i + 1`
done

i=1

echo "until => table of $num is : "

until [ $i -gt 10 ]
do
 res=`expr $num \* $i`
     echo "$res"
     i=`expr $i + 1`
done
 
echo "for => table of $num is : "
for i in {1..10..2}
do
	res=`expr $num \* $i`
	echo "$res"
done
