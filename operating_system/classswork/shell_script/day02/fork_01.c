/*
	 Program to demonstrate fork() sys call
=================================================*/
#include<stdio.h>
#include <sys/types.h>
#include <unistd.h>


int main(void)
{
	printf("main() started !!!\n");

	printf("pid  : %d\n", getpid());//pid of calling process
	printf("ppid : %d\n", getppid());//pid of parent process - shell

	fork();
	//parent process => fork_01.out
	//child process => 

	printf("pid  : %d\n", getpid());//pid of calling process
	printf("ppid : %d\n", getppid());//pid of parent process - shell

	/*
		pid  : 19982 - pid of fork_01.out
		ppid : 19905 - pid of shell/bash

		//this lines executed by parent process
		pid  : 19982 - pid of fork_01.out
		ppid : 19905 - pid of shell/bash
		main() exited !!!

		//this lines executed by child process
		pid  : 19983 - pid of child process
		ppid : 19982 - pid of parent process (fork_01.out)
		main() exited !!!
	*/

	printf("main() exited !!!\n");

	return 0;
}

