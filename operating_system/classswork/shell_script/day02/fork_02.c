#include<stdio.h>
#include<unistd.h>


int main(void)
{
    int ret;

    printf("pid  : %d\n", getpid() );//pid of calling process (fork_03.out)
    printf("ppid : %d\n", getppid() );//pid of shell

    ret = fork();//fork() return value of fork() sys call will be assign
    //after fork() sys call there will be 2 processes running in there independent memory space
    
    if( ret == 0 )//for child process
    {
        printf("child process : ret = %d\n", ret);
        printf("child process : pid  : %d\n", getpid() );
        printf("child process : ppid : %d\n", getppid() );
    }
    else//if( ret != 0 ) => pid of child ===> parent process
    {
        printf("parent process : ret = %d\n", ret);
        printf("parent process : pid  : %d\n", getpid() );
        printf("parent process : ppid : %d\n", getppid() );

    }

    return 0;
}
