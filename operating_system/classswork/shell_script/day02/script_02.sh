#!/bin/bash

clear

PI=3.14

echo -n "enter the radius of circle "

read rad

area=`echo "$PI * $rad * $rad" | bc`

echo "area of the circle is : $area sq. unit"
exit
