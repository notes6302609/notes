#!/bin/bash

clear
echo -n "enter the year : "
read yr

if [ `expr $yr % 4` -eq 0 ]
then
	if [ `expr $yr % 100` -ne 0 ]
	then
		echo "the $yr is leap year"
	elif [ `expr $yr % 400` -eq 0 ]
	then
	 	echo "the $yr is leap year"
	else
			echo "$yr is not a leap year"
	fi
else
	echo "$yr is not a leap year"
fi
