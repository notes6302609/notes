#include<stdio.h>
#include<unistd.h>
#include <sys/wait.h>


int main(void)
{
    int ret;
    int i=0;
        int status;
    printf("main() started!!!");

    ret = fork();

    if( ret == 0 )
    {
        for(i=0;i<20;i++){
            printf("child process : %d\n",i);
            sleep(1);
        }
        _exit(9);
    }
    else
    {
       for(i=0;i<40;i++){
        printf("parent process : %d\n",i);
        sleep(1);

        if( i==20 )
        {
            wait(&status);
             printf("exit value of child process : %d\n", WEXITSTATUS(status));

        } 
       }
    }
    printf("i= %d\n",i);

    return 0;
    //$ps -t pts/0 -m -o pid,ppid,cmd
}