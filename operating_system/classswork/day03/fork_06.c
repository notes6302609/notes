#include<stdio.h>
#include<unistd.h>

int main(void)
{
    int ret;
    int i=0;
    printf("main() started!!!");

    ret = fork();

    if( ret == 0 )
    {
        for(i=0;i<40;i++){
            printf("child process : %d\n",i);
            sleep(1);
        }
    }
    else
    {
       for(i=0;i<20;i++)
        printf("parent process : %d\n",i);
    sleep(1); 
    }
    printf("i= %d\n",i);

    return 0;
    //$ps -t pts/0 -m -o pid,ppid,cmd
}