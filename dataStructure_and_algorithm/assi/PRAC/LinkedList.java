
public class LinkedList {

	static class  Node{
		private Vehicle data;
		private Node next;
		public Node (Vehicle data)
		{
			this.data= data;
			this.next= null;	
		}
	}
	private Node head;
	private int Nodecount;
	
	public LinkedList()
	{
		head=null;
		Nodecount=0;
	}
	public int getNodecount()
	{
		return Nodecount;
	}
	boolean isempty()
	{
		return head==null;
	}
	void addLast(Vehicle data)
	{
		Node newnode= new Node(data);
		if(isempty()) {
			head=newnode;
			Nodecount++;
		}
		else{
				Node trav= head;
			while(trav.next!=null)
			{
				trav=trav.next;
			}
			trav.next=newnode;
			Nodecount++;
		}
		}
	void addFirst(Vehicle data)
	{
		Node newnode= new Node(data);
		if(isempty()) {
			head=newnode;
			Nodecount++;
		}
		else {
			newnode.next=head;
			head= newnode;
			Nodecount++;
		}
	}
	
	void displayLinkeList()
	{
		Node trav=head;
		System.out.println("list is as: ");
		while(trav!=null)
		{	
			System.out.println(trav.data);
			trav=trav.next;
		}
		System.out.println("null");
		System.out.println("no. of Node: "+Nodecount);
	}
	void deleteFirst()
	{
		if(isempty())
			throw new RuntimeException("list is empty!!!!");
		if(head.next==null)
		{	head=null;
			Nodecount--;
		}
		else
		{
			head=head.next;
			Nodecount--;
		}
	}
	void deleteLast()
	{
		if(isempty())
			throw new RuntimeException("List is empty!!!");
		if(head.next==null)
		{
			head=null;
			Nodecount--;
		}
		else
		{
			Node trav=head;
			while(trav.next.next!=null)
			{
				trav=trav.next;
			}
			trav.next=null;
			Nodecount--;
		}
	}
	public void searchVehicle(String numb) {
		if (isempty()) 
		throw new RuntimeException("list is empty");
		
			Node trav = head;
			boolean flag=false;
			while (trav != null) {
				if (trav.data.no.equalsIgnoreCase(numb))
				{flag=true;
					break;}
				trav = trav.next;
			}
			if(flag)
				System.out.println(trav.data);
			else
				System.out.println("vehicle with "+numb+" not found");
				
		
		
	}
	public void displayReverseOrder() {
		if(isempty())
			throw new RuntimeException("list is empty");
	Node trav= head;
		this.displayReverseOrder(trav);
	}
	private void displayReverseOrder(LinkedList.Node trav) {
		if(trav==null)
			return;
		this.displayReverseOrder(trav.next);
		System.out.println(trav.data);
	}
	
	private void swap(Node trav1, Node trav2) {
		
		Vehicle temp= trav1.data;
		trav1.data= trav2.data;
		trav2.data=temp;
		
	}
	public void bubbleSortByprice() 
	{
		if(this.isempty())
			System.out.println("List is empty.");
		else
		{	boolean flag= true;
			Node temp=null;
			
		//	for(int i=1;i<nodecount;i++)
			for(Node itr=head;head!=null;itr=itr.next)
			{	flag=false;
				Node trav1=head;
				Node trav2=trav1.next;
				while(trav2!=temp)
				{
					if(trav1.data.compare(trav2.data))
						{	swap(trav1,trav2);
						flag= true;
						}
				trav1=trav2;
				trav2=trav1.next;
				}
				if(!flag)
					break;
				temp=trav1;
			}
		}
		
	}
}
