import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

public class Graph {
	static class Edge{
		//int u;
		int v;
		int wt;
		Edge(int v,int wt){
			//this.u=u;
			this.v=v;
			this.wt=wt;
		}
	}
	private int vercount;
	private int edgecount;
	private ArrayList<LinkedList<Edge>> list;
	
	Graph(int vercount){
		this.vercount=vercount;
		list= new ArrayList<LinkedList<Edge>>();
		for(int i=0;i<vercount;i++)
			list.add( i,new LinkedList<Edge>());
	}
	void accept() {
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the Edge count");
		this.edgecount=sc.nextInt();
		for(int i=0;i<this.edgecount;i++) {
			System.out.println("Enter zn Edge: from to to & weight");
			int from =sc.nextInt(); int to = sc.nextInt(); int wt= sc.nextInt();
			Edge e1= new Edge(to,wt);
			list.get(from).add(e1);
			
			Edge e2= new Edge(from,wt);
			list.get(to).add(e2);	
		}
	}
	
	void display() {
		   System.out.println("input graph is : ");
	        System.out.println("no. of vertices are : "+vercount);
	        System.out.println("no. of edges are : "+edgecount);
	        
	        for(int i=0;i<list.size();i++)
	        {	LinkedList<Edge> l=list.get(i);
	        	System.out.println();
	        	System.out.print(i+"=>");
	        	for(int j=0;j<l.size();j++)
	        		System.out.print("|"+l.get(j).v+":"+l.get(j).wt+"| -> ");
	        		System.out.print("null");
	        }
	}
	
	
}
