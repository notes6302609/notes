import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		 Scanner sc = new Scanner(System.in);
	        System.out.print("enter no. of vertices : ");
	        int vertCount = sc.nextInt();

	        Graph g1 = new Graph(vertCount);
	        g1.accept();
	        g1.display();        

	}

}
