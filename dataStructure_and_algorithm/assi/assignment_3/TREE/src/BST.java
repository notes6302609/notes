import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class BST {
	static class Node{
		private int data;
		private Node left;
		private Node right;
		private boolean postVisit;
		
		Node(int data)
		{
			this.data=data;
			this.left=null;
			this.right=null;
			this.postVisit= false;
		}
	}
	private Node root;
	BST()
	{
		this.root=null;
	}
	private boolean isempty() {
		
		return root==null;
	}
	void addNode(int data)
	{
		Node newnode= new Node(data);
		if(isempty())
		{
		root=newnode;	
		}
		else
		{
			Node trav=root;
			while(true)
			{	if(data<trav.data) 
				{
					if(trav.left==null)
					{
						trav.left=newnode;
						break;
					}
					else
						trav=trav.left;
					
				}
				else
				{
					if(trav.right==null)
					{
						trav.right=newnode;
						break;
					}
					else
						trav=trav.right;
				}
			}
		}
	}
	
	private boolean searchNode(int data, Node[] ref) {
	//Node parent=ref[0];
	//Node temp=ref[1];
		Node trav=root;
		//System.out.println(ref[0]);
	while(trav!=null) {
		if(data==trav.data)
			{	
			ref[1]=trav;
			return true;
			}
		ref[0]=trav;
		
		if(data<trav.data)
			trav=trav.left;
		else	
			trav=trav.right;
	}
		ref[0]=null;
		ref[1]=null;
		return false;
	}
	boolean deleteNode(int data) {
		Node[] ref= new Node[2];
		if(!searchNode(data,ref))
			return false;
		Node parent=ref[0];
		Node temp=ref[1];
		if(data==root.data) 
			System.out.println(data+" fount at root position");
		else
			System.out.println(data+"found at non root position\n"
		+"parent: "+parent.data);
		
		if(temp.left!=null&&temp.right!=null)
		{
			Node succ = temp.right;
			parent= temp;
			while(succ.left!=null)
			{
				parent= succ;
				succ=succ.left;
			}
			temp.data=succ.data;
			temp=succ;
		}
		
		if(temp.left!=null)
		{
			if(temp==root)
				root=temp.right;
			else if(temp==parent.left)
				parent.left=temp.right;
			else
				parent.right=temp.right;
		}
		else //if(temp.right!=null)
		{
			if(temp==root)
				root=temp.left;
			else if(temp==parent.left)
				parent.left=temp.left;
			else
				parent.right=temp.left;
		}
		return true;
	}
	void dfsTraversal() {
		Stack<Node> s= new Stack<Node>();
		Node trav= root;
		s.push(trav);
		System.out.print("dfs Traversal:\t\t");
		while(!s.empty()) {
			trav=s.peek();s.pop();
			System.out.print(trav.data+" ");
			if(trav.right!=null)
				s.push(trav.right);
			if(trav.left!=null)
				s.push(trav.left);
		}
		System.out.println();
	}
	
	void bfsTraversal() {
		Queue<Node> q= new LinkedList<Node>();
		Node trav= root;
		q.offer(trav);
		System.out.print("bfs Traversal:\t\t");
		while(!q.isEmpty()) {
			trav=q.poll();
			System.out.print(trav.data+" ");
			if(trav.left!=null)
				q.offer(trav.left);
			if(trav.right!=null)
				q.offer(trav.right);
		}
		System.out.println();
	}
	void nonRecPreOrder() {
		Stack<Node> s= new Stack<Node>();
		Node trav=root;
		System.out.print("nonrec Preorder:\t");
		while(trav!=null || !s.empty()) {
			while(trav!=null)
			{
				System.out.print(trav.data+" ");
				if(trav.right!=null)
					s.push(trav.right);
				trav=trav.left;	
			}
			if(!s.empty())
			{
				trav=s.peek(); s.pop();
			}
		}
		System.out.println();
	}
	void nonRecInOrder() {
		Stack<Node> s= new Stack<Node>();
		Node trav=root;
		System.out.print("nonrec Inorder:  \t");
		while(trav!=null || !s.empty()) {
			while(trav!=null)
			{
				s.push(trav);
				trav=trav.left;
			}
			if(!s.empty())
			{
				trav=s.peek(); s.pop();
				System.out.print(trav.data+" ");
			
					trav=trav.right;
			}
		}
		System.out.println();
	}
	void nonRecPostOrder() {
		Stack<Node> s = new Stack<Node>();
		Node trav=root;
		System.out.print("nonerec Postorder:\t");
		while(trav!=null || !s.empty()) {
			while(trav!=null)
			{
				s.push(trav);
				trav=trav.left;
			}
		if(!s.empty())
			{
			trav=s.peek(); s.pop();
				if(trav.right!=null && trav.right.postVisit==false)
				{	
					s.push(trav);
					trav=trav.right;
				}
				else
				{
					System.out.print(trav.data+" ");
					trav.postVisit=true;
					trav=null;
				}
			}
		
		}
	System.out.println();
	}
	
	void recPreOrder()
	{	
		if(isempty())
			throw new RuntimeException("BST is empty");
		System.out.print("Preorder:\t");
		recPreOrder(root);
		System.out.println();
	}
	private void recPreOrder(Node trav) {
		if(trav==null)
			return;
		System.out.print(trav.data+" ");
		recPreOrder(trav.left);
		recPreOrder(trav.right);
		
	}
	
	void recInOrder()
	{	
		if(isempty())
			throw new RuntimeException("BST is empty");
		System.out.print("Inorder:\t");
		recInOrder(root);
		System.out.println();
	}
	private void recInOrder(Node trav) {
		if(trav==null)
			return;
		recInOrder(trav.left);
		System.out.print(trav.data+" ");
		recInOrder(trav.right);
		
	}
	
	void recPostOrder()
	{	
		if(isempty())
			throw new RuntimeException("BST is empty");
		System.out.print("Postorder:\t");
		recPostOrder(root);
		System.out.println();
	}
	private void recPostOrder(Node trav) {
		if(trav==null)
			return;
		recPostOrder(trav.left);
		recPostOrder(trav.right);
		System.out.print(trav.data+" ");
		
	}
	
	private int max(int a, int b) {
		
		return a>b?a:b;
	}
	private int height(Node trav) {
		if(trav==null)
			return -1;
		return max(height(trav.left),height(trav.right))+1;
	}
	int height() {
		if(isempty())
			return -1;
		return height(root);	
	}
}
