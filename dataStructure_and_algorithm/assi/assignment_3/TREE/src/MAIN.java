import java.util.Scanner;

public class MAIN {

	static Scanner sc= new Scanner(System.in);
	public static void main(String[] args) {
	BST t= new BST();
	
	  //Input Order for BST : 50 20 90 85 10 45 30 100 15 75 95 120 5 50
	t.addNode(50);
	t.addNode(20);
	t.addNode(90);
	t.addNode(85);
	t.addNode(10);
	t.addNode(45);
	t.addNode(30);
	t.addNode(100);
	t.addNode(15);
	t.addNode(75);
	t.addNode(95);
	t.addNode(120);
	t.addNode(5);
	t.addNode(50);
	
	
//	t.recPreOrder();
//	t.recInOrder();
//	t.recPostOrder();
//	
//	t.nonRecPreOrder();
//	t.nonRecInOrder();
//	t.nonRecPostOrder();
//	
//	t.dfsTraversal();
//	t.bfsTraversal();
	t.nonRecInOrder();
	System.out.println("heght of tree: "+t.height());
	System.out.println("Enter data which is to be deleted");
	int data= sc.nextInt();
	 if( t.deleteNode(data) )
         System.out.println("node having data part : "+data+" is found in a BST and deleted ....");
     else
         System.out.println("node having data part : "+data+" is not found ....");
		t.nonRecInOrder();
	System.out.println("heght of tree: "+t.height());
	}
}
