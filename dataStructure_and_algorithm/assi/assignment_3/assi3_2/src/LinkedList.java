


public class LinkedList {

	static class  Node{
		private int data;
		private Node next;
		private Node prev; 
		public Node (int data)
		{
			this.data= data;
			this.next= null;
			this.prev=null;
		}
	}
	private Node head;
	private int Nodecount;
	
	public LinkedList()
	{
		head=null;
		Nodecount=0;
	}
	public int getNodecount()
	{
		return Nodecount;
	}
	boolean isempty()
	{
		return head==null;
	}
	void addLast(int data)
	{
		Node newnode= new Node(data);
		if(isempty()) {
			newnode.next=newnode;
			newnode.prev=newnode;
			head=newnode;
			Nodecount++;
		}
		else{
			newnode.prev=head.prev;
			newnode.next=head;
			head.prev.next=newnode;
			head.prev=newnode;
			Nodecount++;
		}
		}
	void addFirst(int data)
	{
		Node newnode= new Node(data);
		if(isempty()) {
			newnode.next=newnode;
			newnode.prev=newnode;
			head=newnode;
			Nodecount++;
		}
		else {
			newnode.prev=head.prev;
			newnode.next=head;
			head.prev.next=newnode;
			head.prev=newnode;
			head= newnode;
			Nodecount++;
		}
	}
	
	void displayLinkeList()
	{
		if (isempty())
			throw new RuntimeException("list is empty!!!!");
		
			Node trav = head;
			System.out.println("list is as: ");
			do {
				System.out.print(trav.data+"->");
				trav = trav.next;
			} while (trav != head);
			System.out.println("null");
			System.out.println("no. of Node: " + Nodecount);
		
	}
	void deleteFirst()
	{
		if(isempty())
			throw new RuntimeException("list is empty!!!!");
	
	 if(head==head.next){
			head=null;
			Nodecount--;	
		}
	 else{	head.next.prev=head.prev;
			head=head.next;
			head.prev.next=head;
			Nodecount--;
		}
	}
	void deleteLast()
	{
		if(isempty())
			throw new RuntimeException("List is empty!!!");
		if(head==head.next)
		{
			head=null;
			Nodecount--;
		}
		else
		{	head.prev=head.prev.prev;
			head.prev.next=head;
			Nodecount--;
		}
	}
	public void searchint(int string) {
		if (isempty()) 
		throw new RuntimeException("list is empty");
		
			Node trav = head;
			boolean flag=false;
			 do{
				if (trav.data==string)
				{flag=true;
					break;}
				trav = trav.next;
			}while (trav != head);
			if(flag)
				System.out.println(trav.data);
			else
				System.out.println("int with "+string+" not found");
				
		
		
	}
	

	public void displayReverseOrder() {
		if(isempty())
			throw new RuntimeException("list is empty");
	Node trav= head;
	System.out.println("reverse list is as: ");
	do {
		trav=trav.prev;
		System.out.println(trav.data+"->");
	}while(trav!=head);
	System.out.println("null");
	System.out.println("no. of nodes: "+Nodecount);
	}
	private void swap(Node trav1, Node trav2) {
		
		int temp= trav1.data;
		trav1.data= trav2.data;
		trav2.data=temp;
		
	}
	public void bubbleSortByprice() 
	{
		if(this.isempty())
			System.out.println("List is empty.");
		else
		{	boolean flag= true;
			Node temp=null;
			
			
			for(int i=0;i<Nodecount;i++)
			{	flag=false;
				Node trav1=head;
				Node trav2=trav1.next;
				for(int j=0;j<Nodecount-i;j++)
				{
					if(trav1.data>(trav2.data))
						{	swap(trav1,trav2);
						flag= true;
						}
				trav1=trav2;
				trav2=trav1.next;
				}
				if(!flag)
					break;
				temp=trav1;
			}
			System.out.println("linked list is sorted");
		}
		
	}
	
}


