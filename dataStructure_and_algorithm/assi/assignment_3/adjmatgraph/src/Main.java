import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
	
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter no. of vertices");
		AdjMatgraph g1= new AdjMatgraph(sc.nextInt());
		g1.acceptGraph();
		g1.displayGraph();
	}
	/*

	9
14

0 1 4
0 7 8
1 2 8
1 7 11
2 3 7
2 5 4
2 8 2
3 4 9
3 5 14
4 5 10
5 6 2
6 7 1
6 8 6
7 8 7

	*/
}
