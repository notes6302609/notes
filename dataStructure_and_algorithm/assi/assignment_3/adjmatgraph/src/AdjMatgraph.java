import java.util.Scanner;

public class AdjMatgraph {
private int[] [] mat;
private int vercount;
private int edgecount;

AdjMatgraph(int vercount){
	this.vercount=vercount;
	this.mat=new int[vercount][vercount];
	for(int i=0;i<mat.length;i++)
		for(int j=0;j<mat.length;j++)
			mat[i][j]=0;
	
}

void acceptGraph(){
	Scanner sc= new Scanner(System.in);
	System.out.println("Enter no. of edges");
	this.edgecount=sc.nextInt();
	for(int v=0; v<edgecount;v++)
	{	System.out.println("Enter an edge from to to: ");
		int from= sc.nextInt();
		int to= sc.nextInt();
		mat[from][to]=1;
		mat[to][from]=1;
	}
	
}
void displayGraph(){
    System.out.println("input graph is : ");
    System.out.println("no. of vertices are : " + this.vercount);
    System.out.println("no. of edges are: "+ this.edgecount);
    System.out.println("============================================");
    for( int i = 0 ; i < this.vercount ; i++ ){
        for( int j = 0 ; j < this.vercount ; j++ ){
            System.out.printf("%4d", mat[ i ][ j ]);
        }
        System.out.println();
    }
    System.out.println();
    System.out.println("============================================");
}

}
