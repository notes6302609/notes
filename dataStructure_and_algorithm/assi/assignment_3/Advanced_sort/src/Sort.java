
public class Sort {
   public static void displayArrayElements(int [] arr){
        System.out.print("array ele's are: ");
        for( int index = 0 ; index < arr.length ; index++ ){
            System.out.printf("%4d", arr[ index ] );
        }
        System.out.println();
    }
   private static void swap(int[] arr, int i, int j) {
	   int t= arr[i];
	   arr[i]=arr[j];
	   arr[j]=t;   
   }
public static void quickSort(int[] arr, int left, int right) {
	if(left>=right)
		return ;
	int i= left;
	int j= right;
	int pivot= arr[left];
	
	while(i<j) {
		while(i<=right && arr[i]<=pivot)
			i++;
		while(arr[j] > pivot)
			j--;
		if(i<j)
			swap(arr,i,j);
	}
	swap(arr,left,j);
	
	quickSort(arr,left,j-1);
	quickSort(arr,j+1,right);
}

public static void merge(int[] arr, int left, int mid, int right) {
	int size= right-left+1;
	int [] temp= new int[size];
	int i= left;
	int j= mid+1;
	int k=0;
	
	while(i<=mid && j<=right)
	{
		if(arr[i]<arr[j])
			temp[k++]=arr[i++];
		else
			temp[k++]=arr[j++];	
	}
	while(i<=mid)
		temp[k++]=arr[i++];
	while(j<=right)
		temp[k++]=arr[j++];	
	k=0;
	i=left;
	while(i<=right)
	arr[i++]=temp[k++];
	
}
public static void mergeSort(int[] arr, int left, int right) {
	if(left>=right)
		return;
	int mid =(left+right)/2;
	
	mergeSort(arr,left,mid);
	
	mergeSort(arr,mid+1,right);
	merge(arr,left,mid,right);
}
	public static void main(String[] args) {
//int [] arr = { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
        
        //int [] arr = { 50, 90, 20, 100, 20, 40, 10, 80, 30, 70 };
        
        int [] arr = { 30, 20, 60, 50, 10, 40 };

        System.out.print("bfore sorting: "); displayArrayElements(arr);
        //quickSort(arr, 0, arr.length-1);//initialization
        mergeSort(arr,0,arr.length-1);
        System.out.print("after sorting: "); displayArrayElements(arr);

	}

}
