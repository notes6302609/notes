import org.w3c.dom.Node;

public class LinkedList {

	static class  node{
		private int data;
		private node next;
		public node (int data)
		{
			this.data= data;
			this.next= null;	
		}
	}
	private node head;
	private int nodecount;
	
	public LinkedList()
	{
		head=null;
		nodecount=0;
	}
	public int getNodecount()
	{
		return nodecount;
	}
	boolean isempty()
	{
		return head==null;
	}
	void addLast(int data)
	{
		node newnode= new node(data);
		if(isempty()) {
			head=newnode;
			nodecount++;
		}
		else{
				node trav= head;
			while(trav.next!=null)
			{
				trav=trav.next;
			}
			trav.next=newnode;
			nodecount++;
		}
		}
	void addFirst(int data)
	{
		node newnode= new node(data);
		if(isempty()) {
			head=newnode;
			nodecount++;
		}
		else {
			newnode.next=head;
			head= newnode;
			nodecount++;
		}
	}
	void addAtPos(int data,int pos)
	{
		if(pos==1) {
			addFirst(data);
		}
		else if(pos==nodecount+1)
		{
			addLast(data);
		}
		else
		{	node newnode= new node(data);
			node trav= head;
			for (int i = 1; i < pos-1; i++)
					trav = trav.next; 
			newnode.next=trav.next;
			trav.next=newnode;
			
			nodecount++;
			
		}
		
	}
	
	void displayLinkeList()
	{
		node trav=head;
		System.out.println("list is as: ");
		while(trav!=null)
		{	
			System.out.print(trav.data+"->");
			trav=trav.next;
		}
		System.out.println("null");
		System.out.println("no. of node: "+nodecount);
	}
	void deleteFirst()
	{
		if(isempty())
			throw new RuntimeException("list is empty!!!!");
		if(head.next==null)
		{	head=null;
			nodecount--;
		}
		else
		{
			head=head.next;
			nodecount--;
		}
	}
	void deleteLast()
	{
		if(isempty())
			throw new RuntimeException("List is empty!!!");
		if(head.next==null)
		{
			head=null;
			nodecount--;
		}
		else
		{
			node trav=head;
			while(trav.next.next!=null)
			{
				trav=trav.next;
			}
			trav.next=null;
			nodecount--;
		}
	}
	
	void deleteAtPos(int pos)
	{
		if(pos==1)
		{
			this.deleteFirst();
		}
		else if(pos==nodecount)
		{
			this.deleteLast();
		}
		else
		{
			node trav=head;
			for(int i=1;i<pos-1;i++)
			{
				trav=trav.next;
			}
			trav.next=trav.next.next;
			nodecount--;
		}
	}
	
	private void swap(LinkedList.node trav1, LinkedList.node trav2) {
	
		int temp= trav1.data;
		trav1.data= trav2.data;
		trav2.data=temp;
		
	}
	void selectionSort() {
		for(node trav1=head; trav1.next!=null;trav1=trav1.next)
		{	
			for(node trav2=trav1.next;trav2!=null;trav2=trav2.next)
			{	
				if(trav1.data>trav2.data)
				swap(trav1,trav2);	
			}
	
		}
		
	}
//	void bubblesort()
//	{
//		for(int i=1;i<nodecount;i++)
//		{ node trav1= head;
//			node trav2=trav1.next;
//			for(int j=1;j<nodecount-i;j++)
//			{	
//				if(trav1.data>trav2.data)
//					swap(trav1,trav2);
//				trav1=trav2;
//				trav2=trav1.next;
//			
//			}
//			
//		}
//	}

	public void bubbleSort() 
	{
		if(this.isempty())
			System.out.println("List is empty.");
		else
		{	boolean flag= true;
			node temp=null;
			
		//	for(int i=1;i<nodecount;i++)
			for(node itr=head;head!=null;itr=itr.next)
			{	flag=false;
				node trav1=head;
				node trav2=trav1.next;
				while(trav2!=temp)
				{
					if(trav1.data>trav2.data)
						{	swap(trav1,trav2);
						flag= true;
						}
				trav1=trav2;
				trav2=trav1.next;
				}
				if(!flag)
					break;
				temp=trav1;
			}
		}
		
	}
	public void merge( LinkedList l)
	{
		this.bubbleSort();
		l.bubbleSort();
		node trav=this.head;
		while(trav.next!=null)
			trav=trav.next;
		trav.next=l.head;
		
	}
	public void insertsort(int data)
	{
		node newnode= new node(data);
		node trav= head;
		boolean flag =false;
		if(head==null)
			head=newnode;
		else{
			while(trav.next!=null){
				if(newnode.data<head.data)
				{	newnode.next=head;
					head=newnode;
					flag=true;
					break;
				}
				else if(trav.data<=newnode.data&& trav.next.data>newnode.data)
			{
				newnode.next=trav.next;
				trav.next=newnode;
				 flag = true;
				 break;
			}
			trav=trav.next;
		}
		if(!flag)
			trav.next=newnode;
		}
	}
	
	
	void removeDuplicate()
	{
		this.bubbleSort();
		node trav1= head;
		node trav2=trav1.next;
		while(trav2!=null)
		{	if(trav1.data==trav2.data)
				{trav1.next=trav2.next;
				nodecount--;
				}
			trav1=trav1.next;
			trav2=trav1.next;
		}
	}
	
	void reverseLinkedList()
	{
		node trav1= head;
		node trav2= trav1.next;
		node trav3;
		trav1.next=null;
		while(trav2!=null)
		{
			trav3=trav2.next;
			trav2.next=trav1;
			trav1=trav2;
			trav2=trav3;
		}
		head=trav1;
	}
	void reverseInGroup()
	{
		node trav1=head;
		node trav2=trav1.next;
		node trav3;
		trav1.next=null;
		boolean flag=false;
		while(trav2!=null){
			for(int i=0;i<3;i++)
			{System.out.println("inside for loop");
				trav3=trav2.next;
			trav2.next=trav1;
			trav1=trav2;
			trav2=trav3;
			if(trav2==null)
				break;
			}
			if(!flag)
			{
				head=trav1;
				flag=true;
			}
	
	}
		System.out.println("reverse in group succefull");
		this.displayLinkeList();
}
}

