import java.util.Scanner;

public class assi3_1 {
	static Scanner sc= new Scanner(System.in);
	public static void menuDriven(LinkedList l1) {
		int choice;
		do {
			System.out.println();
			System.out.println("0.\texit");
			System.out.println("1.\tadd node at last position");
			System.out.println("2.\tadd node at first position");
			System.out.println("3.\tadd node at given position");
			System.out.println("4.\tdisplay list");
			System.out.println("5.\tdelete node at last position");
			System.out.println("6.\tdelete node at first position");
			System.out.println("7.\tdelete node at given position");
			System.out.println("8.\tsort linklist using selection sort");
			System.out.println("9.\tsort linklist using bubble sort");
			System.out.println("10.\tadd node in a sorted manner");
			System.out.println("11.\treverse the linkedlist");
			System.out.println("12.\tremove the duplicate elements from linkedlist");
			System.out.println("13.\treverse in a group of 3");
			System.out.println("enter your choice");
			choice= sc.nextInt();
			int data;
			switch(choice)
			{
			default: 
				System.out.println("invalid input");
				break;
			case 0:			
				break;
			case 1:
				System.out.println("enter the data ");
				//data=sc.nextInt();
				l1.addLast(sc.nextInt());
				break;
			case 2:
				System.out.println("enter the data");
				l1.addFirst(sc.nextInt());
				break;
			case 3:
				while(true) {
					System.out.println("enter the position : ");
					int pos=sc.nextInt();
					if(pos>=1&&pos<=l1.getNodecount()+1)
					{	System.out.println("enter the data");
					l1.addAtPos(sc.nextInt(), pos);
					break;
					}
					System.out.println("invalid position!!!");
				}
				break;
			case 4:
				l1.displayLinkeList();
				break;
				
				
			case 5:
				try {
					l1.deleteLast();
				}
				catch(RuntimeException e) {
					System.out.println(e.getMessage());
				}
				break;
			case 6:
				try {
					l1.deleteFirst();
				}
				catch(RuntimeException e) {
					System.out.println(e.getMessage());
				}
				break;
				
			case 7:
				while(true) {
					System.out.println("enter the position : ");
					int pos=sc.nextInt();
					if(pos>=1&&pos<=l1.getNodecount()+1)
					{	try {
						l1.deleteAtPos(pos);
					}
					catch(RuntimeException e)
					{
						System.out.println(e.getMessage());
					}
					break;
					}
					System.out.println("invalid position!!!");
				}
				break;
				
			case 8:
				l1.selectionSort();
				
				break;
			case 9:
				l1.bubbleSort();
				break;	
			case 10:
				System.out.println("enter the data");
				l1.insertsort(sc.nextInt());
				break;
			case 11:
				l1.reverseLinkedList();
				System.out.println("linkedLIst reversed");
				break;
			case 12:
				l1.removeDuplicate();
				System.out.println();
				break;
			case 13:
				l1.reverseInGroup();
				break;
			}	
			
		}while(choice!=0);
	}
	public static void main(String args[])
	{	
		LinkedList l1= new LinkedList();
	System.out.println("linklist 1");
		menuDriven(l1);
//		System.out.println("linklist 2");
//		LinkedList l2= new LinkedList();
//		menuDriven(l2);
//		System.out.println("******merging two list*******");
//	l1.merge(l2);	
//	menuDriven(l1);
	}
}
