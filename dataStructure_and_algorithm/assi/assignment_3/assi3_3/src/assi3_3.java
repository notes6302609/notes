import java.util.Arrays;
import java.util.Stack;

public class assi3_3 {

	private static int priority(String ele) {
		
		switch(ele) {
		default :
			return -1;
		case "(":
			return 0;
		case "+":
		case "-":
			return 1;
		case "*":
		case "/":
		case "%":
			return 2;
		case "$":
		case "^":
			return 3;
		}
	}
	private static boolean isoperand(String cur) {
		
		return !(cur.equals("+")||cur.equals("-")||cur.equals("/")||cur.equals("%")||
				cur.equals("*")||cur.equals("$")||cur.equals("^")||cur.equals("(")||cur.equals(")"));
	}
	
	private static String infixToPostfix(String infix) {
		Stack<Character> s= new Stack<Character>();
		StringBuffer postfix = new StringBuffer();
		for(int i=0;i<infix.length();i++)
		{
			Character cur = infix.charAt(i);
			
			if(isoperand(String.valueOf(cur)))
			{
				postfix.append(cur);
			}
			else if(cur=='(')
			{
				s.push(cur);
			}
			else if(cur==')')
			{
				while(!(s.peek()=='('))
				{
					postfix.append(s.peek());
					s.pop();
				}
				s.pop();
			}
			else {	
				while(!s.empty()&&priority(s.peek().toString())>=priority(cur.toString()))
				{
					postfix.append(s.peek()); s.pop();
				}
				s.push(cur);
			}
		}
		while(!s.empty())
		{
			postfix.append(s.peek());
			s.pop();
		}
		return postfix.toString();
	}
public static String infixtoPrefix(String infix) {
	Stack<Character> s= new Stack<Character>();
	StringBuilder prefix=new StringBuilder();
	for(int i=infix.length()-1;i>=0;i--)
	{	Character cur=infix.charAt(i);
		if(isoperand(cur.toString()))
		{
			prefix.append(cur);
		}
		else
		{
			while(!s.empty()&& priority(s.peek().toString())>priority(cur.toString()))
					{
						prefix.append(s.peek());
						s.pop();
					}
			s.push(cur);
		}
	}
		while(!s.empty())
		{
			prefix.append(s.peek());
			s.pop();
		}
	
	return prefix.reverse().toString();
}

public static int postfixEvaluation(String[] postfix)
{	
	Stack<Integer> s= new Stack<Integer>();
	int result ;
	for(int i=0;i<postfix.length;i++) {
	String cur= postfix[i];
	if(isoperand(cur))
	{
		s.push(Integer.parseInt(cur));
	}
	else
	{
		int op2=s.peek(); s.pop();
		int op1=s.peek(); s.pop();
		result= (int) cal(op1,op2,cur);
		s.push(result);
	}
	}
	result=s.peek();
	return result;
}
	private static double cal(int op1, int op2, String opr) {
		switch(opr) {
		default :
			return -1;
		case "+": return op1+op2;
		case "-":return op1-op2;
		case "*":return op1*op2;
		case "/":return op1/op2;
		case "%":return op1%op2;
		case "$":
		case "^":return Math.pow(op1, op2);
		}
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//String infix="a*b/c*d+e-f*g+h";
		
		//String infix="a*b/c*d+pqr-f*g+h";
		 // String infix = "4+5*9/7+3-6";
//		  String infix = "((a+b)/(c*d)+e)-f*g+h";
//		System.out.println("infix: "+infix);
//		System.out.println("postfix: "+infixToPostfix(infix));
//		infix="a*b/c*d+e-f*g+h";
//		System.out.println("infix: "+infix);
//		System.out.println("postfix: "+infixToPostfix(infix));
		
//		String infix="a*b/c*d+e-f*g+h";
//		System.out.println("infix: "+infix);
//		System.out.println("prefix: "+infixtoPrefix(infix));
		
		String[] postfix={"25","45","12","/","17","*","+","100","+"};
		
		System.out.println("postfix: "+Arrays.toString(postfix));
		System.out.println("postfix evaluation: "+postfixEvaluation(postfix));
		
	}


}
