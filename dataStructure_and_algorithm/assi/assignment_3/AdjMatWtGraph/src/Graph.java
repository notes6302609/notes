import java.util.Scanner;

public class Graph {
	private int[] [] mat;
	private int vercount;
	private int edgecount;
	private static final int INF=999999;
	Graph(int vercount){
		this.vercount=vercount;
		this.mat=new int[vercount][vercount];
		for(int i=0;i<mat.length;i++)
			for(int j=0;j<mat.length;j++)
				mat[i][j]=INF;
		
	}

	void acceptGraph(){
		Scanner sc= new Scanner(System.in);
		System.out.println("Enter no. of edges");
		this.edgecount=sc.nextInt();
		for(int v=0; v<edgecount;v++)
		{	System.out.println("Enter an edge from to to: and weight ");
			int from= sc.nextInt();
			int to= sc.nextInt();
			int weight= sc.nextInt();
			mat[from][to]=weight;
			mat[to][from]=weight;
		}
		
	}
	void displayGraph(){
	    System.out.println("input graph is : ");
	    System.out.println("no. of vertices are : " + this.vercount);
	    System.out.println("no. of edges are: "+ this.edgecount);
	    System.out.println("============================================");
	    for( int i = 0 ; i < this.vercount ; i++ ){
	        for( int j = 0 ; j < this.vercount ; j++ ){
	           if(mat[i][j]!=INF) 
	        	System.out.printf("%4d", mat[ i ][ j ]);
	           else
	        	   System.out.printf("%4s","#");
	        }
	        System.out.println();
	    }
	    System.out.println();
	    System.out.println("============================================");
	}
	
	void dijkstras(int source){
		int[] dist= new int[this.vercount];
		boolean [] marked=new boolean[vercount];
		int sptvertices=0;
		for(int v=0; v<this.vercount ;v++)
		{
			dist[v]=INF;
			marked[v]=false;
		}
		dist[source]=0;
		int[] spt= new int[this.vercount];
	
		while(sptvertices < vercount){
			int minkeyvert=getMinkeyVertex(dist,marked);
			marked[minkeyvert]=true;
			spt[sptvertices++]=minkeyvert;
			for(int v=0;v<vercount;v++) {
			if(mat[minkeyvert][v]!=INF && marked[v]==false && dist[v]> dist[minkeyvert]+mat[minkeyvert][v])
				dist[v]= dist[minkeyvert]+mat[minkeyvert][v];
			}
		}
		System.out.print("SPT Set of Vertices is :\n { ");
		for(int i: spt)
			System.out.print(i+" ");
		System.out.println("}");
		 System.out.println("distance of all vertices from given source vertex is : ");
	        for( int v = 0 ; v < vercount ; v++ )
	            System.out.printf("distance[ %d ] from %d is : %d\n", v, source, dist[ v ] );
	
	}
	public void prims(int root) {
		int[] vertexKey= new int[this.vercount];
		int[] parent= new int[this.vercount];
		boolean [] marked=new boolean[vercount];
		
		int mstcount=0;
		for(int v=0; v<this.vercount ;v++)
		{
			vertexKey[v]=INF;
			marked[v]=false;
			parent[v]=-1;
		}
		vertexKey[root]=0;
		parent[root]=root;
		int[] mst= new int[this.vercount];
	
		while(mstcount < vercount){
			int minkeyvert=getMinkeyVertex(vertexKey,marked);
			marked[minkeyvert]=true;
			mst[mstcount++]=minkeyvert;
			for(int v=0;v<vercount;v++) {
				if(mat[minkeyvert][v]!=INF && marked[v]==false && vertexKey[v]> mat[minkeyvert][v]){
					vertexKey[v]= mat[minkeyvert][v];
					parent[v]=minkeyvert;
				}
			}
		}
		System.out.print("MST Set of Vertices is :\n { ");
		for(int i: mst)
			System.out.print(i+" ");
		System.out.println("}");
		 int weightMST = 0;
		for( int i = 0 ; i < vercount ; i++ ){
	            if( i != root ){
	                System.out.println(i +" -> "+ parent[i] + " : " + mat[ i ][ parent[i] ]);
	                weightMST += mat[ i ][ parent[i] ];
	            } 
	        }

	        System.out.println("Weight of MST is : "+weightMST);
	
	}
	

	 private int getMinkeyVertex(int[] dist, boolean[] marked) {
	 
		int minindex=0;
		int mindist=INF;
		for(int i=0;i<vercount;i++)
		{
			if(marked[i]==false&& dist[i]<mindist)
				{
				minindex=i;
				mindist=dist[i];
				}
		}
		
		return minindex;
	}

}
