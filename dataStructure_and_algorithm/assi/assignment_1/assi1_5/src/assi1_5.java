import java.util.Scanner;

public class assi1_5 {
public static String hex(int n)
{
	char[] c= {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	
	if(n==0)
		return "";
	return hex(n/16)+ c[n%16]; 
			
}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("enter no. for hexadecimal conversion");
		Scanner sc= new Scanner(System.in);
		int num=sc.nextInt();
		System.out.println("hexadecimal of num is "+hex(num));
	
	}
	/*
	 27/16   1     11
	 1/16    0      1==>1
	 1+11==>b
	 1b
	 */

}
