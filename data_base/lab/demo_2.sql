drop procedure if exists sp_add;
delimiter $$
create procedure sp_add(in v_num int)
begin
declare v_num1 int default v_num;
declare v_sum int default 0;
declare v_res int;
while v_num1 !=0 do
set v_res=floor(v_num1%10);
set v_sum=v_sum+v_res;
set v_num1=floor(v_num1/10);
end while;
insert into temp values(v_num,v_sum);
end;
$$
delimiter ;