Drop procedure if exists sp_sum_even;
Delimiter $$
create procedure sp_sum_even(in p_low int, in p_high int)
Begin
declare v_sum double default 0;

    while p_low<=p_high do

    if p_low%2=0 then
        set v_sum=v_sum+p_low;
    End if;
     set p_low=p_low+1;
    End while;

    insert into result values(v_sum,"sum of even no");
End;

$$

delimiter ;