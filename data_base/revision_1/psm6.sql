Drop procedure if exists sp_prime;
delimiter $$
create procedure sp_prime(in p_num int)
Begin
declare v_num int default 2;
declare v_status char(10);

    prime:LOOP
        if v_num = p_num then
            set v_status ="prime";
            leave prime;
        end if;

        if p_num%v_num=0 then
            set v_status = "not prime";
             leave prime;
        end if;
        set v_num= v_num +1;
    End LOOP;
    select concat(p_num," is a ",v_status) as  Status;
    
end;
$$

delimiter ;