```sql
create database rationcard;

create table RationCardTb(
    RationNo int primary key identity(1,1),
    PersonName varchar(15),
    Age int,
    Address varchar(50),
    ContactNumber varchar(12),
    Status varchar(15),
    Role varchar(20),
    Email varchar(30),
    Password varchar(15)
);

insert into rationcardtb(personname,Age,Address,ContactNumber,status,Role,Email,password)
values('saurabh',23,'sambhaginagar','12345','processing','Admin','saurabh@gmail.com','test1234');

insert into rationcardtb(personname,Age,Address,ContactNumber,status,Role,Email,password)
values('Name1',21,'Address1','12345','processing','Applicant','name1@gmail.com','test1234');

insert into rationcardtb(personname,Age,Address,ContactNumber,status,Role,Email,password)
values('Name2',22,'Address2','12345','processing','Applicant','name2@gmail.com','test1234');

insert into rationcardtb(personname,Age,Address,ContactNumber,status,Role,Email,password)
values('Name3',23,'Address3','12345','processing','Applicant','name3@gmail.com','test1234');

insert into rationcardtb(personname,Age,Address,ContactNumber,status,Role,Email,password)
values('Name4',24,'Address4','12345','processing','Applicant','name4@gmail.com','test1234');

```
