Drop procedure if exists sp_rectArea;
delimiter $$
Create procedure sp_rectArea(in P_len int,in p_bre int)
Begin
declare v_area double default 0;
declare id int;

-- set v_area = p_bre * P_len;
select p_bre*P_len into v_area;
insert into result values(v_area,"rect area");

end;
$$

delimiter ;