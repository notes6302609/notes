Drop procedure if exists sp_even_odd;

delimiter $$
create procedure sp_even_odd(in p_num int )
Begin
declare v_status char(30);

    if p_num % 2 = 0 then
    set v_status = "is a even";
    else
    set v_status = " is a odd";
    end if;

select concat(p_num,' -',v_status) as Status;
insert into result values(p_num,v_status);
end;



$$

delimiter ;