drop procedure if exists sp_square1;
delimiter $$
create procedure sp_square1(inout p_sq int)
Begin
set p_sq=p_sq*p_sq;
end;
$$
delimiter ;