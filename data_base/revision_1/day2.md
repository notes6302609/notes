```sql
insert into temp values('ab','ab','ab');

select upper(left(ename,1)) from emp;

select lower(substring(ename,2)) from emp;

select concat( upper(left(ename,1)),lower(substring(ename,2))) from emp;

select Left('1234 5678 9123 4567',5);
select rpad('1234 ',9,'x');
select concat(Rpad(Left('1234 5678 9123 4567',5),9,'X'),' ', lpad(Right('1234 5678 9123 4567',5),9,'X'));


select e.ename,d.dname,a.tal,a.dist from emps e inner join depts d on e.deptno= d.deptno
inner join addr a on e.empno= a.empno;

select e.ename, m.topic from emps e inner join emp_meeting em on e.empno=em.empno
inner join meeting m on m.meetno= em.meetno
  ;

 select e.ename, m.topic,a.tal,a.dist from emps e inner join emp_meeting em on e.empno=em.empno
inner join meeting m on m.meetno= em.meetno
inner join addr a on  a.empno=e.empno
inner join depts d on e.deptno=d.deptno;


select * from emps;
select deptno,count(*) from emps e group by deptno;
select d.dname,count(e.empno) from emps e right join depts d on e.deptno=d.deptno group by d.dname;

select e.ename , count(em.meetno) from emps e inner join emp_meeting em on e.empno=em.empno group by e.ename order by 2 desc;

select e.ename from emps e inner join depts d on e.deptno=d.deptno where d.dname='dev';

select max(sal) from emp;
select ename from emp where sal = (select max(sal) from emp);

select distinct sal from emp order by sal desc;
select distinct sal from emp order by sal desc limit 1,1;

select ename ,sal from emp where sal = (select distinct sal from emp order by sal desc limit 1,1);
select dept from emp where ename = 'king';
select ename from emp where deptno = (select deptno from emp where ename = 'king') and ename <> 'king';

select job from emp where ename = 'scott';
select * from emp where job= (select job from emp where ename = 'scott') and ename <> 'scott';

select sal from emp where job='salesman';
select * from emp where sal > All(
select sal from emp where job='salesman');

select sal from emp where deptno=20;
select * from emp where sal< ANY(select sal from emp where deptno=20);

select distinct deptno from emp;
select dname from depts where deptno =any(select distinct deptno from emp);

select dname from dept where deptno <>any(select distinct deptno from emp);
```
