drop procedure if exists sp_circle_area;
delimiter $$
create procedure sp_circle_area()
begin
declare area double default 0.0;
declare radius int default 5 ;
set area= pi()*radius*radius;
select area ; 
end;
$$
delimiter ;