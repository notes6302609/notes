drop procedure if exists sp_recarea;
delimiter $$
create procedure sp_recarea(in p_len int,in p_bre int )
begin
declare v_area double;
select p_bre*p_len into v_area;
insert into result values(v_area,"area of rec"); 
end;
$$
delimiter ;