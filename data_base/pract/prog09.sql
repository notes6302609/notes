drop function if exists amstrong;
delimiter $$
create function amstrong(p_num int)
returns char(40)
deterministic 
begin
declare v_temp int;
declare v_num1 int default p_num;
declare v_res int default 0;
declare v_final char(40);
while v_num1>0 do
set v_temp=v_num1%10;
set v_res=v_res+pow(v_temp,length(p_num));
set v_num1=trunc(v_num1/10);
end while;
if v_res=p_num then 
set v_final= 'no. is amstrong';
else
set v_final='no. is not amstrong';
end if;
return v_final;
end;
$$
delimiter ;