drop procedure if exists sp_square;
delimiter $$
create procedure sp_square(in p_num int,out p_sqr int)
begin
set p_sqr=p_num*p_num;
end;
$$
delimiter ;