drop procedure if exists sp_even_odd;
delimiter $$
create procedure sp_even_odd(in p_num int)
begin
declare v_temp char(20);
if p_num<0 then
set v_temp='no. is negative'; 
elseif p_num%2=0 then 
set v_temp ='even';
else 
set v_temp='odd';
end if;
select v_temp as even_odd;
end;
$$
delimiter ;