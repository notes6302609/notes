drop procedure if exists sp_facto;
delimiter $$
create procedure sp_facto(inout p_num int,inout res int)
begin
set res=res*p_num;
set p_num=p_num-1;
if p_num!=0 then 
call sp_facto(p_num,res);
end if;
select res;
end;
$$
delimiter ;