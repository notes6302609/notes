 //1. Given  
public class Fruit{}
public class Melon extends Fruit{}
public class WaterMelon extends Melon{}

//Which of the following, will be legal ?

1.List<? extends Fruit> fruits=new ArrayList<Fruit>();
2.List<? extends Fruit> fruits=new ArrayList<Melon>();
3.List<? extends Fruit> fruits=new LinkedList<WaterMelon>();
4.List<? extends Fruit> fruits=new Vector<Object>();
5.List<? super Melon> melons=new ArrayList<Fruit>();
6.List<? super Melon> melons=new LinkedList<>();
7.List<? super Melon> melons=new LinkedList<WaterMelon>();