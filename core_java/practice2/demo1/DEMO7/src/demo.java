
class Base {
	int x;
}

class Derived extends Base {
	int y;

	public Derived() {
		// TODO Auto-generated constructor stub
		System.out.println("inside derived ctor");
	}

	public Derived(int y) {

		this.y = y;
		System.out.println("inside derived param ctor");
	}

}

public class demo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Derived d = new Derived();
		Derived d1 = new Derived(3);
		System.out.println(d1.x);
	}

}
