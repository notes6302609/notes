
class A{}

class B extends A {}

class C extends B{}



public class demo3 {  
    public static void main(String[] args)   
    {   
    A a = new A(); 
    System.out.println(a.getClass());
    A a1 = new B();
    System.out.println(a1.getClass());
    
    A a2 = new C();
    System.out.println(a2.getClass());
    
    C c = new C();
    System.out.println(c.getClass());
    
    }   
}  