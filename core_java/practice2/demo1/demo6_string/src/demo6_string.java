
public class demo6_string {

	public static void main(String[] args) {
		String s1 = "Hello";
		String s2 = "India";
		String s3 = s1 + s2;
		String s4 = s1.concat(s2);
		String s5= "HelloWorld";
		String s6="Hello"+"World";
		System.out.println(s3==s5);
		System.out.println(s4==s5);
		System.out.println(s5==s6);
		System.out.println(s3==s4);
		System.out.println(s3.equals(s4));

	}

}
