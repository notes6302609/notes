package lab_prac;

import java.util.Objects;

public class Cricketer implements Comparable<Cricketer> {

private String name,email_id,phone;
private int age,rating;
public Cricketer() {
	super();
	// TODO Auto-generated constructor stub
}
public Cricketer(String name, String email_id, String phone, int age, int rating) {
	super();
	this.name = name;
	this.email_id = email_id;
	this.phone = phone;
	this.age = age;
	this.rating = rating;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail_id() {
	return email_id;
}
public void setEmail_id(String email_id) {
	this.email_id = email_id;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public int getRating() {
	return rating;
}
public void setRating(int rating) {
	this.rating = rating;
}

@Override
public int hashCode() {
	return Objects.hash(rating);
}
@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Cricketer other = (Cricketer) obj;
	return rating == other.rating;
}


@Override
public String toString() {
	return "Cricketer [name=" + name + ", email_id=" + email_id + ", phone=" + phone + ", age=" + age + ", rating="
			+ rating + "]";
}
@Override
public int compareTo(Cricketer o) {
	if(this.rating > o.rating)
	return 1;
	else if(this.rating<o.rating)
		return -1;
	else
		return 0;
}}
