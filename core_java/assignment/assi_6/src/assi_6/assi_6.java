package assi_6;

import java.util.Scanner;
//. Write a program to perform matrix multiplication in java.
public class assi_6 {
	static Scanner sc= new Scanner(System.in); 

	public static void main(String[] args) {
	System.out.println("Developer: D5_62986_Saurabh_Waghchoure");
	int [][]m1= {{1,2,3},{4,5,6},{7,8,9}};
	int [][]m2= {{1,2,3,},{4,5,6},{7,8,9}};
	int [][]m3=new int[3][3];
	for(int r1=0;r1<3;r1++)
	{
		for(int c2=0;c2<3;c2++)
		{
			m3[r1][c2]= 0;
			for(int z=0;z<3;z++)
			{
				m3[r1][c2]+=m1[r1][z]*m2[z][c2];
				
			}
			
			System.out.print(m3[r1][c2]+"\t");
		}
		System.out.println();
	}
	
}

}
