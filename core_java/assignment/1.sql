Drop function if exists Armstrong;
Delimiter $$
create function Armstrong(p_num INT)
returns char(50)
Deterministic
Begin
Declare v_status Char(40);
Declare v_sum Int Default 0;
Declare v_len Int;
Declare v_num Int Default p_num;
declare v_result Char(50);
declare v_s Int;
set v_len = length(p_num);

while v_num!=0 Do
set v_num = p_num % 10 ;
set v_sum = v_sum + pow(v_num, v_len);
set v_num = v_num / 10 ;
End while;

/*
if v_sum = p_num then
set v_s = 1;
else 
set v_s = 0;
End if;
select(concat(p_num , v_result)) into v_result;
return v_s;
*/


if v_sum = p_num then
set v_status = 'is Armstrong number' ;
else 
set v_status = 'is not armstrong' ;
End if;
select(concat(p_num , v_result)) into v_result;
set v_result = concat(p_num , v_status);
return v_result;

End;
$$
Delimiter ;