package assi_4;

import java.util.Scanner;

class marks
{
	int math;
	int sci;
	int eng;
	int mar;
	int hist;
	Scanner sc = new Scanner(System.in) ;	
	public void acce_marks()
	{
	System.out.println("enter the marks of math");
	this.math=sc.nextInt();
	System.out.println("enter the marks of science");
	this.sci=sc.nextInt();
	System.out.println("enter the marks of english");
	this.eng=sc.nextInt();
	System.out.println("enter the marks of marathi");
	this.mar=sc.nextInt();
	System.out.println("enter the marks of history");
	this.hist=sc.nextInt();	
	}
	public void dis_marks() 
	{
		System.out.println("marks in math = "+this.math);
		System.out.println("marks in science = "+this.sci);
		System.out.println("marks in english = "+this.eng);
		System.out.println("marks in marathi = "+this.mar);
		System.out.println("marks in history = "+this.hist);
	}
	public int total() {
		return (this.math+this.sci+this.eng+this.mar+this.eng+this.hist);
	}	
}

class student{
	String name;
	int Roll_no;
	marks m= new marks();
	Scanner sc = new Scanner(System.in) ;	
	public void acce_record()
	{
		System.out.println(" enter name and roll no");
		this.name= sc.next();
		this.Roll_no=sc.nextInt();
		m.acce_marks();	
	}
	public void dis_record()
	{
		System.out.println("name : "+this.name+"\troll no : "+this.Roll_no);
		m.dis_marks();	
	}
	public char grade (int n) 
	{ 
	  if(n>=90)
		  return 'A';
	  else if (90>n&&n>= 80)
		  return 'B';
	  else if (80>n&&n>=70)
		  return 'C';
	  else if (70>n&&n>=60)
		  return 'D';
	  else 
		  return 'F';
		}
}

public class assi_4 {
	public static void main(String[] args)
	{	
		System.out.println("Developer: D5_62986_Saurabh_Waghchoure");
		student s = new student ();
		s.acce_record();
		s.dis_record();
		int total=s.m.total();
		int avg = total/5;

		System.out.println("grade in math "+s.grade(s.m.math));
		System.out.println("grade in science"+s.grade(s.m.sci));
		System.out.println("grade in english "+s.grade(s.m.eng));
		System.out.println("grade in marathi "+s.grade(s.m.mar));
		System.out.println("grade in history "+s.grade(s.m.hist));
		System.out.println("average grade "+s.grade(avg));
		
	}

}
