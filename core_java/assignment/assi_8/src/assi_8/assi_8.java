package assi_8;

import java.util.Scanner;
//Write a Java Application to read the name of a student (studentName),
//roll Number (rollNo) and marks (totalMarks) obtained. rollNo may be an
//alphanumeric string. Display the data as read

class marks
{
	int math;
	int sci;
	int eng;
	Scanner sc = new Scanner(System.in) ;
	public void acce_marks()
	{
		System.out.println("enter the marks of math");
		this.math=sc.nextInt();
		System.out.println("enter the marks of science");
		this.sci=sc.nextInt();
		System.out.println("enter the marks of english");
		this.eng=sc.nextInt();
	}
	public void dis_marks() 
	{
		System.out.println("marks in math = "+this.math);
		System.out.println("marks in science = "+this.sci);
		System.out.println("marks in english = "+this.eng);
	
	}
	public int total() {
		return (this.math+this.sci+this.eng);
	}
}

class student{
	String name;
	String Roll_no;
	marks m= new marks();
	Scanner sc = new Scanner(System.in) ;
	public void acce_record()
	{
		System.out.println(" enter full name of student" );
		this.name= sc.nextLine();
		System.out.println(" enter Alphanumeric roll no.  of student" );
		this.Roll_no=sc.next();
		m.acce_marks();	
	}
	public void dis_record()
	{
		System.out.println("name : "+this.name+"\troll no : "+this.Roll_no);
		m.dis_marks();	
	}	
}

public class assi_8 {
	public static void main(String[] args)
	{	System.out.println("Developer: D5_62986_Saurabh_Waghchoure");
		student s = new student ();
		s.acce_record();
		s.dis_record();
		System.out.println("total mark obtained = "+s.m.total());
		System.out.println("average mark obtained = "+(s.m.total())/3);
	}

}
