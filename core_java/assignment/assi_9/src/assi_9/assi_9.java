package assi_9;

import java.util.Scanner;
//Accept a integer number and when the program is executed print the
//binary, octal and hexadecimal equivalent of the given number.
//Sample Output:
//java Test
//Enter Number : 20
//Given Number :20
//Binary equivalent :10100
//Octal equivalent :24
//Hexadecimal equivalent :14
public class assi_9 {
	public static long binary(int n)
	{ long bin=0;
	for(int i=1;n!=0;i*=10)
	{
		int rem=n%2;
		n/=2;
		bin+=rem*i;	
	}
		return bin;
	}
public static void main(String args[])
{
	System.out.println("Developer: D5_62986_Saurabh_Waghchoure");
	Scanner sc= new Scanner(System.in);
	System.out.print("Enter Number : ");
	int n=sc.nextInt();
	System.out.println("Given Number : "+n);
	System.out.println("Binary equivalent : "+assi_9.binary(n));
	System.out.printf("Octal equivalent : %o",n);
	System.out.printf("\nhexadecimal equivalent : %x",n);
	
	
	
}
}
