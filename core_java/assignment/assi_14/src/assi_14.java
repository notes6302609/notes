import java.util.Scanner;
//Write a user-defined function to display the letters present in the given string in reverse order.
//The function neither try to modify given string nor return it. Hint: Use recursion to display
//characters in reverse order.
public class assi_14 {
	static void reverse(String str)
	{
		for(int i=str.length()-1;i>=0;i--)
		{
			System.out.print(" "+str.charAt(i));
		}
	}
	static Scanner sc= new Scanner(System.in);
	public static void main(String[] args) {
		System.out.println("Developer: D5_62986_Saurabh_Waghchoure");
		System.out.println("Enter the String");
		String str = new String(sc.next());
		reverse(str);
	}

}
